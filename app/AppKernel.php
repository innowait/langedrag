<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new AppBundle\AppBundle(),
            new AdminBundle\AdminBundle(),
            new LanguageBundle\LanguageBundle(),
            new CatalogBundle\CatalogBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new UserBundle\UserBundle(),
            new Gregwar\ImageBundle\GregwarImageBundle(),
            new StaticPageBundle\StaticPageBundle(),
            new ContactBundle\ContactBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new MenuBundle\MenuBundle(),
            new ApplicationBundle\ApplicationBundle(),
            new ProjectBundle\ProjectBundle(),
            new Innowait\HealthBundle\InnowaitHealthBundle(),
            new Sensio\Bundle\BuzzBundle\SensioBuzzBundle(),
            new Innowait\FeedbackBundle\InnowaitFeedbackBundle(),
            new Innowait\OrderBundle\InnowaitOrderBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
