$(document).ready(function(){
	'use strict';
	//Login Register Validation
	if($("form.form-horizontal").attr("novalidate")!=undefined){
		$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	}

	// Remember checkbox
	if($('.chk-remember').length){
		$('.chk-remember').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
		});
	}

	$("form[name='auth']").on('submit', function (e) {
		e.preventDefault();
		e.stopPropagation();
		$.ajax({
			type: "POST",
			url: $("form[name='auth']").attr('action'),
			dataType: 'json',
			data: {
				_username: $('input[name="_username"]').val(),
				_password: $('input[name="_password"]').val(),
				_remember_me: $('input[name="_remember_me"]').is(":checked"),
				_csrf_token: $('input[name="_csrf_token"]').val()
			}
		}).success(function(data) {
			if (data.success) {
				document.location.href = data.link;
			} else {
				swal("Oops!", data.message, "error");
			}
		}).fail(function(data) {
			console.log(data);
		});
	})
});
