var CookiesComponent = {
    cookieKey: 'LANGEDRAG-COOKIES-ACCEPTED',
    html: '<div class="c-cookies">' +
        '<div class="container">' +
        '<div class="row">' +
        '<div class="col-lg-1 col-md-1 d-none d-lg-block"><div class="c-cookies--icon">_ICON_</div></div>' +
        '<div class="col-lg-7 col-md-8 col-12"><div class="c-cookies--caption">_TEXT_</div></div>' +
        '<div class="col-lg-4 col-md-3 col-12"><div class="c-cookies--button"><div class="c-button c-cookies--button-agree">_TITLE_</div></div></div>' +
        '</div>' +
        '</div>' +
        '</div>',
    icon: '<i class="fas fa-eye"></i>',
    textLoaded: false,
    text: 'We use cookies to provide you best customer experience ever! We use it to help you get most of our company, so if you continue using this website - you agree to this.<span class="d-none d-lg-block"> We do not and will not sell your information to anyone in no possible scenarios.</span>',
    title: '',
    init: function () {
        var me = this;
        var value = me.getState();
        if (value == undefined && me.validScreen()) {
            setTimeout(() => {
                me.showCookiesPopup();
            }, 1500);
        }
        $(window).resize(function () {
            var value = me.getState();
            if (!me.validScreen() || value) {
                me.hideCookiesPopup();
            } else {
                me.showCookiesPopup();
            }
        });
        $(document).delegate(".c-cookies--button-agree", "click", function () {
            me.hideCookiesPopup();
            me.stopShowingCookies();
        });
    },
    stopShowingCookies: function () {
        var me = this;
        window.localStorage.setItem(me.cookieKey, true);
        SystemComponent.logEvent('click', 'Privacy', 'Accept Cookies');
    },
    validScreen: function () {
        return $(window).width() > 1;
    },
    hideCookiesPopup: function () {
        $(".c-cookies").remove();
    },
    showCookiesPopup: function () {
        var me = this;
        me.loadText()
            .then(() => {
                var html = me.html;
                html = html.replace(/_ICON_/, me.icon).replace(/_TEXT_/, me.text).replace(/_TITLE_/, me.title);
                $("body").append(html);
            });
    },
    loadText: function() {
        let me = this;
        return new Promise((success, error) => {
            if (me.textLoaded) {
                success();
            }
            $.ajax({
              url: $("body").data('cookies'),
              success: function (response) {
                  if (response.success) {
                      me.text = response.content;
                      me.title = response.title;
                      me.textLoaded = true;
                      success();
                  }
              }
            });
        });
    },
    getState: function () {
        var me = this;
        return window.localStorage.getItem(me.cookieKey);
    }
};
$(document).on('app:ready', function () {
    CookiesComponent.init();
});