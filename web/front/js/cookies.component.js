var CookiesComponent = {
    cookieKey: 'TRAVONTO-COOKIES-ACCEPTED',
    html: '<div class="c-cookies">' +
        '<div class="container">' +
        '<div class="row">' +
        '<div class="col-lg-1 col-md-1 d-none d-lg-block"><div class="c-cookies--icon">_ICON_</div></div>' +
        '<div class="col-lg-7 col-md-8 col-8"><div class="c-cookies--caption">_TEXT_</div></div>' +
        '<div class="col-lg-4 col-md-3 col-4"><div class="c-cookies--button"><div class="btn btn-outline-light c-cookies--button-agree">_BUTTON_</div></div></div>' +
        '</div>' +
        '</div>' +
        '</div>',
    icon: '<i class="fas fa-eye fa-3x"></i>',
    text: 'We use cookies to provide you best customer experience ever! We use it to help you enjoy every second of your trips, so if you continue using this website - you agree to this.<span class="d-none d-lg-block"> We do not and will not sell your information to anyone in no possible scenarios.</span>',
    init: function () {
        return true;
        var me = this;
        var value = me.getState();
        if (value == undefined && me.validScreen()) {
            me.showCookiesPopup();
        }
        $(window).resize(function () {
            var value = me.getState();
            if (!me.validScreen() || value) {
                me.hideCookiesPopup();
            } else {
                me.showCookiesPopup();
            }
        });
        $(document).delegate(".c-cookies--button-agree", "click", function () {
            me.hideCookiesPopup();
            me.stopShowingCookies();
        });
    },
    stopShowingCookies: function () {
        var me = this;
        window.localStorage.setItem(me.cookieKey, true);
        // SystemComponent.logEvent('click', 'Privacy', 'Accept Cookies');
    },
    validScreen: function () {
        return $(window).width() > 1;
    },
    hideCookiesPopup: function () {
        $(".c-cookies").remove();
    },
    showCookiesPopup: function () {
        var me = this;
        var html = me.html;
        let htmlBlock = $("html");
        html = html
            .replace(/_ICON_/, me.icon)
            .replace(/_TEXT_/, htmlBlock.data('cookie'))
            .replace(/_BUTTON_/, htmlBlock.data('cookie-button'));
        $("body").append(html);
    },
    getState: function () {
        var me = this;
        return window.localStorage.getItem(me.cookieKey);
    }
};
$(document).ready(function () {
    CookiesComponent.init();
});