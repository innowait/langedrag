let ServicesComponent = {
    init: function () {
        $(".c-services--service").click(function () {
            let serviceBlock = $(this);
            $(".c-services--service__active").removeClass('c-services--service__active');
            serviceBlock.addClass('c-services--service__active');
            let id = serviceBlock.data('id');

            if ($(".c-services--content__active").length > 0) {
                $(".c-services--content__active").fadeOut(400, function () {
                    $(".c-services--content__active").removeClass('c-services--content__active');
                    $(".c-services--content[data-id='"+id+"']").fadeIn(400, function () {
                        $(".c-services--content[data-id='"+id+"']").addClass('c-services--content__active');
                    });
                });
            } else {
                $(".c-services--content[data-id='"+id+"']").fadeIn().addClass('c-services--content__active');
            }
        });
        setTimeout(function () {
            if ($(".c-services--service__preselected").length > 0) {
                $(".c-services--service__preselected").click();
            } else {
                $(".c-services--service").first().click();
            }
        }, 500);
    }
};

$(document).ready(function () {
    ServicesComponent.init();
});