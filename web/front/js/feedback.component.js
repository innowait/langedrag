let FeedbackComponent = {
    init: function () {
        $(".c-footer--message-button").click(function () {

            let button = $(this);
            button.hide();

            $(".c-footer--form").toggleClass('c-footer--form__opened');
            $(".contact-form").each((index, form) => {
                form = $(form);
                if (form.hasClass('modal-form')) {
                    return;
                }

                let formHeight = form.height();
                let totalFormHeight = $(".c-footer--form").height();
                console.log('changing height to ' + totalFormHeight + ' ' + formHeight);
                form.css({
                    'margin-top': (totalFormHeight - formHeight) / 2
                });
            });
        });
        $(".c-footer--form--close").click(function () {
            $(".c-footer--form").toggleClass('c-footer--form__opened');
            setTimeout(() => {
                $(".c-footer--message-button").show();
            }, 300);
        });
    }
};

$(document).ready(function () {
    FeedbackComponent.init();
});