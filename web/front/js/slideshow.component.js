let SlideShowComponent = {
    init: function () {
        let me = this;
        me.updateSize();
        $(window).resize(function () {
            me.updateSize();
        });
        let domain = 'https://langehouse.se';
        me.supportWebP()
            .then(supported => {
                if ($(window).width() < $(window).height()) {
                    $(".c-slideshow--slide").each(function () {
                        let slide = $(this);
                        let background = slide.find('.c-slide--background');
                        background.css({
                            'background-image': 'url('+domain + (supported && background.data('mobile-webp') !== undefined ? background.data('mobile-webp') : background.data('mobile')) +')'
                        });
                    });
                } else {
                    $(".c-slideshow--slide").each(function () {
                        let slide = $(this);
                        let background = slide.find('.c-slide--background');
                        background.css({
                            'background-image': 'url('+ domain + (supported && background.data('webp') !== undefined ? background.data('webp') : background.data('image'))+')'
                        });
                    });
                }
            })
        $(".owl-carousel").each(function () {
            let item = $(this);

            item.owlCarousel({
                items: item.data('items') ? parseInt(item.data('items')) : 1,
                margin: item.data('margin') ? parseInt(item.data('margin')) : 0,
                stagePadding: item.data('padding') ? parseInt(item.data('padding')) : 0,
                animateOut: item.data('out') ? item.data('out') : false,
                animateIn: item.data('in') ? item.data('in') : false,
                autoplay: item.data('auto') !== undefined ? item.data('auto') === 'true' : true,
                responsive: item.data('responsive') !== undefined ? item.data('responsive') : false,
                loop: true,
                center: true,
                dots: item.data('dots') ? true : false,
                nav: item.data('nav') ? true : false,
                navText: ['<img src="./front/media/images/icon/left-chevron.png" width="100%">', '<img src="./front/media/images/icon/right-chevron.png" width="100%">']
            });
        });
    },
    supportWebP: function() {
        let testContent = "UklGRhoAAABXRUJQVlA4TA0AAAAvAAAAEAcQERGIiP4HAA==";
        return new Promise((success, error) => {
            var img = new Image();
            img.onload = function () {
                success((img.width > 0) && (img.height > 0));
            };
            img.onerror = function () {
                success(false);
            };
            img.src = "data:image/webp;base64," + testContent;
        });
    },
    updateSize: function () {
        $(".c-slideshow").height($(window).height());
        $(".c-slide").height($(window).height());
        $(".c-map").height($(window).height() - 30);
        $(".c-slideshow .owl-carousel").trigger('refresh.owl.carousel');
    }
};

$(document).on("app:ready", function () {
    SlideShowComponent.init();
});