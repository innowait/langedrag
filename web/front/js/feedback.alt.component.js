let FeedbackComponent = {
    init: function () {
        if ($(".c-section__services").length > 0) {
            // $(".c-footer--form").css({
            //     top: '60px'
            // });
        }
        $(window).resize(function () {
            if ($(".c-footer__active").length > 0) {
                if ($(window).width() > 800) {
                    $(".c-footer__active").width($(".c-services--menu").offset().left + $(".c-services--menu").width() + 40);
                } else {
                    $(".c-footer__active").width('100%');
                }
            }
        });
        $(".c-footer--message-button").click(function () {
            let button = $(this);
            button.hide();

            if (button.hasClass('c-footer--message-button__scroll')) {
                $(document).scrollTop(0);
            }

            $(".c-footer").addClass('c-footer__active');
            if ($(window).width() > 800) {
                $(".c-footer__active").width($(".c-services--menu").offset().left + $(".c-services--menu").width() + 40);
            } else {
                $(".c-footer__active").width('100%');
            }
            $(".c-footer--form").toggleClass('c-footer--form__opened');
        });
        $(".c-footer--form--close").click(function () {
            $(".c-footer--form").toggleClass('c-footer--form__opened');
            setTimeout(() => {
                $(".c-footer--message-button").show();
                $(".c-footer").removeClass('c-footer__active');
                $(".c-footer").width(0);
            }, 300);
        });

        let formHeight = $(".contact-form").height();
        let totalFormHeight = $(".c-footer--form").height();
        $(".contact-form").css({
            'margin-top': (totalFormHeight - formHeight) / 2
        });
    }
};

$(document).ready(function () {
    FeedbackComponent.init();
});