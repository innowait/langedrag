let GalleryComponent = {
    init: function () {
        let me = this;
        $(document).delegate(".c-gallery", "click", function () {
            let item = $(this);
            $(".gallery-carousel").trigger('to.owl.carousel', item.data('item'));
        });

        me.refit();

        $(window).resize(function () {
            me.refit();
        });
    },
    refit: function () {
        let windowHeight = $(window).height();
        let windowWidth = $(window).width();

        if (windowHeight > windowWidth) {
            $(".c-gallery").height($(window).height() / 2);
        } else {
            $(".c-gallery").height($(window).height() * 0.75);
        }

        let minMargin = 200;
        if ($(window).width() < 1600) {
            minMargin = 100;
        }
        if ($(window).width() < 1200) {
            minMargin = 0;
        }
        if ($(window).width() > 1400) {
            minMargin += ($(window).width() - 1400) / 2;
        }
        $(".c-gallery").css({
            'margin': '0 ' + (minMargin) +'px'
        });
        $(".gallery-carousel .owl-prev").css({ 'left': (minMargin)  });
        $(".gallery-carousel .owl-next").css({ 'right': (minMargin) });

        $(".gallery-carousel").trigger('refresh.owl.carousel');
    }
};

$(document).ready(function () {
    GalleryComponent.init();
});