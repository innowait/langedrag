let MenuComponent = {
    menuNode: null,
    init: function () {
        let me = this;
        $(".c-menu--items--item__burger").click(() => {
            $(".c-menu-full").addClass('c-menu-full__opened');
            me.alignMenuSize();
        });
        $(".c-menu-full--close").click(() => {
            $(".c-menu-full").removeClass('c-menu-full__opened');
        });
        this.menuNode = $(".c-menu");
        this.checkFixed();
        $(window).scroll(() => {
            this.checkFixed();
        });
        $(".c-section__services").scroll(() => {
            this.checkFixed();
        });
        $(window).resize(() => {
            if ($(".c-menu-full__opened").length > 0) {
                me.alignMenuSize();
            }
        });
    },
    alignMenuSize: function() {
        let height = (($(window).height() - $(".c-menu-full--items").height())/2) + 20;
        if ($(window).width() < 800) {
            height += 20;
        }

        $(".c-menu-full--items").css({
            'margin-top': height
        });
    },
    checkFixed: function () {
        let currentScroll = $(document).scrollTop();
        let servicesScroll = $(".c-section__services").scrollTop();
        if (currentScroll > 80 || servicesScroll > 0) {
            this.menuNode.addClass('c-menu__fixed');
        } else {
            this.menuNode.removeClass('c-menu__fixed');
        }
    }
};

$(document).ready(function () {
    MenuComponent.init();
});