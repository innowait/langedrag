var SystemComponent = {
    init: function () {
        let me = this;
        document.addEventListener("touchstart", function() {}, true);
        $(".c-footer--top").click(function () {
            $("html, body").animate({ scrollTop: 0 }, "slow");
        });
        me.animateItems();
        me.lazyLoadImages();
        me.addMetaDataForImages();
        $(window).scroll(function () {
            me.animateItems();
            me.lazyLoadImages();
            me.addMetaDataForImages();
        });
        setTimeout(() => {
            $(".c-intro").fadeOut('slow');
            $(document).trigger('app:ready');
        }, 3000);
    },
    addMetaDataForImages: function() {
        setTimeout(() => {
            $("img").each(() => {
                let image = this;
                if (image.hasClass('meta-added')) {
                    return;
                }
                image.addClass('meta-added');
                if (image.attr('alt') === undefined) {
                    image.attr('alt', 'Langedrag Housing Generic Image');
                }
                if (image.attr('title') === undefined) {
                    image.attr('title', 'Langedrag Housing Generic Image');
                }
            });
        }, 1500)
    },
    supportWebP: function() {
        let testContent = "UklGRhoAAABXRUJQVlA4TA0AAAAvAAAAEAcQERGIiP4HAA==";
        return new Promise((success, error) => {
            var img = new Image();
            img.onload = function () {
                success((img.width > 0) && (img.height > 0));
            };
            img.onerror = function () {
                success(false);
            };
            img.src = "data:image/webp;base64," + testContent;
        });
    },
    lazyLoadImages: function() {
        let threshold = 100;
        let me = this;
        me.supportWebP()
            .then(supported => {
                $(".lazy-img").each(function () {
                    let imageElement = $(this);
                    if (imageElement.hasClass('processed')) {
                        return;
                    }
                    let offset = imageElement.offset().top - $(window).height() - threshold;
                    let type = imageElement.data('type');
                    if (type == undefined) {
                        type = 'img';
                    }
                    let src = '';
                    if (supported && imageElement.data('webp') !== undefined) {
                        src = imageElement.data('webp');
                    } else {
                        src = imageElement.data('src');
                    }
                    if (offset > 0 && $(window).scrollTop() > offset || imageElement.hasClass('fast')) {
                        //console.log("[!] Loading: Source: " + src + " :: Current Offset: " + $(window).scrollTop() + " :: Image Offset: " + offset);
                        switch (type) {
                            case 'img':
                                imageElement.attr('src', src);
                                break;
                            case 'background':
                                imageElement.css({
                                    'background-image': 'url('+src+')'
                                });
                                break;
                        }
                        imageElement.addClass('processed');
                    }
                });
            });
    },
    animateItems: function () {
        let me = this;
        var currentScroll = $(window).scrollTop() + $(window).height() - 100;
        $(".js-animate").each(function () {
            let item = $(this);
            if (item.hasClass('js-animate__opened')) {
                return;
            }
            let itemScroll = item.offset().top;
            console.log('Current position: ' + currentScroll);
            console.log('Item position: ' + itemScroll);

            if (itemScroll < currentScroll) {
                console.log('REVEAL');
                item.addClass('js-animate__opened');
            }
        });
    }
};

$(document).ready(function () {
    SystemComponent.init();
});