let SectionComponent = {
    init: function () {
        let me = this;
        this.updateSections();
        $(window).resize(function () {
            me.updateSections();
        });
       $(document).on("app:ready", function () {
        me.updateSections();
        });
    },
    updateSections: function () {
        $(".c-section.c-section__full").height($(window).height() - 100);

        $(".c-section.c-section__full").each(function () {
            let section = $(this);
            let content = section.find('.c-section--internal');
            let paddingTop = ((section.height() - content.height()) / 2);
            let overallSectionHeight = (paddingTop * 2 + content.height());
            let contentHeight = content.height();
            console.log('Section START --------');
            console.log(section);
            console.log('Initial padding:' + paddingTop);
            console.log('Content height:' + contentHeight);
            console.log('Overall height:' + overallSectionHeight);
            console.log('Section END --------');
            if (paddingTop * 2 + contentHeight > section.height()) {
                paddingTop = paddingTop / 2;
            }
            if (paddingTop > 0) {
                content.css({
                    'padding-top': parseInt(paddingTop) + 'px'
                });
            } else {
                content.css({
                    'padding-top': 0 + 'px'
                });
            }
        });
    }
};

$(document).ready(function () {
    SectionComponent.init();
});