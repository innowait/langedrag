let FeedbackComponent = {
    init: function () {
        $(window).resize(function () {
            if ($(".c-footer__active").length > 0) {
                $(".c-footer__active").width('50%');
            }
        });
        $(".c-footer--message-button").click(function () {
            let button = $(this);
            button.hide();
            $(".c-footer").addClass('c-footer__active');
            if ($(window).width() > 800) {
                $(".c-footer__active").width('50%');
            } else {
                $(".c-footer__active").width('100%');
            }
            $(".c-footer--form").toggleClass('c-footer--form__opened');
        });
        $(".c-footer--form--close").click(function () {
            $(".c-footer--form").toggleClass('c-footer--form__opened');
            setTimeout(() => {
                $(".c-footer--message-button").show();
                $(".c-footer").removeClass('c-footer__active');
                $(".c-footer").width('0');
            }, 300);
        });
        $(".contact-form").each((form) => {
            if (form.hasClass('modal-form')) {
                return;
            }
            console.log(form);
            let formHeight = form.height();
            let totalFormHeight = form.find(".c-footer--form").height();
            form.css({
                'margin-top': (totalFormHeight - formHeight) / 2
            });
        });
    }
};

$(document).ready(function () {
    FeedbackComponent.init();
});