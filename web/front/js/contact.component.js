var ContactComponent = {
    sending: false,
    init: function () {
        let me = this;
        $(".send-message").click(function () {
            let button = $(this);
            let form;
            if (button.hasClass('modal-form')) {
                form = $(".contact-form.modal-form");
            } else {
                form = button.parent();
            }
            console.log(form);
            let errors = false;
            if (me.sending) {
                return;
            }
            form.find(".c-required__error").removeClass('c-required__error');
            form.find(".c-required").each(function () {
                let input = $(this);
                if (input.val() == '') {
                    input.addClass('c-required__error');
                    errors = true;
                }
            });
            if (errors) {
                return;
            }
            button.html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            me.sending = true;
            $.ajax({
                url: form.data('url'),
                data: {
                    name: form.find("input[name='name']").val(),
                    email: form.find("input[name='email']").val(),
                    phone: form.find("input[name='phone']").val(),
                    message: form.find("textarea[name='message']").val()
                },
                success: function (response) {
                    me.sending = false;
                    if (response.success) {
                        button
                            .html(response.message)
                            .addClass('btn-success');
                    }
                }
            });
        });
    }
};

$(document).ready(function () {
    ContactComponent.init();
});