<?php
namespace Deployer;

const APP_NAME = 'langedrag';
const APP_REPO = 'git@bitbucket.org:innowait/langedrag.git';
CONST APP_SCHEMA = 'https';
CONST APP_HOST = 'langehouse.se';


require 'recipe/symfony.php';

// Project name
set('application', APP_NAME);

// Project repository
set('repository', APP_REPO);

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', ['app/logs', 'app/cache', 'web/uploads', 'web/tmp']);

// Writable dirs by web server 
add('writable_dirs', []);
set('allow_anonymous_stats', false);


host('209.97.137.183')
    ->set('labels', ['stage' => 'production'])
    ->set('user', 'root')
    ->set('deploy_path', '/usr/share/nginx/innowait.cms/' . APP_NAME);
//host('innowait-web-01')
//    ->stage('staging')
//    ->user('root')
//    ->set('deploy_path', '/usr/share/nginx/innowait.cms/' . APP_NAME . '_stage');

task('config:prod', function () {
    $appName = "    application: " . APP_NAME;
    $dbName = "    database_name: innowait_" . APP_NAME;
    run("cd {{release_path}} && sed -i 's/%APP%/'".APP_NAME."'/g' ./app/deploy/server.config");
    run("cd {{release_path}} && cp ./app/deploy/server.config /etc/nginx/sites-enabled/innowait.cms." . APP_NAME);
    run("service nginx reload");
    run('cd {{release_path}} && echo -e "\n'. $appName .'" >> ./app/deploy/prod.yml');
    run('cd {{release_path}} && echo -e "'. $dbName .'" >> ./app/deploy/prod.yml');
    run('cd {{release_path}} && cp ./app/deploy/prod.yml ./app/config/parameters.yml');
});

task('sitemap:generate', function () {
    run('cd {{release_path}} && php app/console app:sitemap:generate --host=' . APP_HOST . ' --schema=' . APP_SCHEMA);
});

task('database:create', function () {
//    run('cd {{release_path}} && php app/console doctrine:database:create');
});

//task('script:deploy', function () {
//    run('cd {{release_path}} && ./vendor/bin/phing deploy');
//});

after('deploy:failed', 'deploy:unlock');

before('deploy:vendors', 'config:prod');
after('deploy:vendors', 'database:create');
after('deploy:vendors', 'sitemap:generate');

//after('deploy:vendors', 'script:deploy');
