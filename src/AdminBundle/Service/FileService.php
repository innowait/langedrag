<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 05/07/2017
 * Time: 00:09
 */

namespace AdminBundle\Service;


use Gregwar\ImageBundle\Services\ImageHandling;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileService
{
    const WEB_DIR = '/../../../web/uploads/';

    private $webDir;
    /**
     * @var ImageHandling
     */
    private $imageManipulator;

    private $imageThumbs = [
        'template-block' => [
            'thumb' => [
                'width' => 800,
                'height' => 600,
                'watermark' => false
            ]
        ],
        'testimonial' => [
            'thumb' => [
                'width' => 450,
                'height' => 450,
                'watermark' => false
            ]
        ],
        'staff' => [
            'thumb' => [
                'width' => 500,
                'height' => 500,
                'watermark' => false
            ]
        ],
        'catalog-category' => [
            'bg' => [
                'width' => 1700,
                'height' => 875,
                'watermark' => false,
                'filters' => [
                    'brightness' => -40
                ]
            ]
        ],
        'slide' => [
            'bg' => [
                'width' => 1920,
                'height' => 1080,
                'watermark' => false
            ]
        ],
        'mobile_slide' => [
            'bg' => [
                'width' => 1080,
                'height' => 1920,
                'watermark' => false
            ]
        ],
        'gallery' => [
            'bg' => [
                'width' => 1920,
                'height' => 1080,
                'watermark' => false
            ]
        ],
        'static-page' => [
            'bg' => [
                'width' => 1700,
                'height' => 875,
                'watermark' => false,
                'filters' => [
                    'brightness' => -40
                ]
            ]
        ],
        'catalog-item' => [
            'thumb' => [
                'width' => 800,
                'height' => 534,
                'watermark' => false
            ],
            'large' => [
                'width' => 800,
                'height' => 800,
                'watermark' => false
            ],
            'small' => [
                'width' => 500,
                'height' => 500,
                'watermark' => false
            ]
        ],
        'showroom' => [
            'thumb' => [
                'width' => 450,
                'height' => 450,
                'watermark' => false
            ],
            'large' => [
                'width' => 800,
                'height' => 800,
                'watermark' => false
            ],
        ]
    ];

    public function __construct(ImageHandling $imageHandling)
    {
        $this->webDir = __DIR__ . self::WEB_DIR;
        $this->imageManipulator = $imageHandling;
    }

    public function uploadFile(UploadedFile $file, $folder = 'general')
    {
        $name = time() . '_' . mt_rand(1000,9999) . '.' . strtolower($file->getClientOriginalExtension());
        if (!is_dir($this->webDir)) {
            mkdir($this->webDir);
            chmod($this->webDir, 0777);
        }
        $file->move($this->webDir . $folder, $name);

        foreach ($this->imageThumbs[$folder] as $typeName => $config) {
            $image = $this->imageManipulator->open($this->webDir . $folder . '/' . $name);

            $image->zoomCrop($config['width'], $config['height']);

            if ($config['watermark']) {
                $image->write(
                    $this->webDir . '../front/fonts/Caviar_Dreams_Bold.ttf',
                    $config['watermark']['label'],
                    $x = $config['watermark']['x'],
                    $y = $config['watermark']['y'],
                    $size = $config['watermark']['size'],
                    $angle = $config['watermark']['angle'],
                    $color = $config['watermark']['color'],
                    $align = $config['watermark']['align']);
            }

            if (isset($config['filters'])) {
                foreach ($config['filters'] as $filterName => $filterValue) {
                    switch ($filterName) {
                        case 'brightness':
                            $image->brightness($filterValue);
                            break;
                    }
                }
            }
            $image->save($this->webDir . $folder . '/' . $typeName . '_' . $name, 'guess', 90);
        }

        return $name;
    }
}