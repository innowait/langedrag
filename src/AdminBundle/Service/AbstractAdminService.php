<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 21:57
 */

namespace AdminBundle\Service;


use AdminBundle\Model\AdminFilter;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use LanguageBundle\Service\LanguageService;
use MenuBundle\Entity\Menu;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Language;

class AbstractAdminService implements AdminServiceInterface
{
    protected $listTitle = 'Admin List';
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var string
     */
    protected $repoName;

    public $templates = [];

    /**
     * @var LanguageService
     */
    protected $languageService;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @var FileService
     */
    protected $fileService;

    public function __construct(EntityManager $entityManager, $repoName, LanguageService $languageService, FileService $fileService)
    {
        $this->entityManager = $entityManager;
        $this->repoName = $repoName;
        $this->repository = $this->entityManager->getRepository($repoName);
        $this->languageService = $languageService;
        $this->fileService = $fileService;
    }

    public function getItem(AdminFilter $filter)
    {
        if ($filter->getId()) {
            /** @var Menu $item */
            return $this->repository->findOneBy(['id' => $filter->getId()]);
        }
        return false;
    }

    public function getItems(AdminFilter $filter)
    {
        return $this->repository->findAll();
    }

    public function deleteItem(AdminFilter $filter)
    {
        $item = $this->getItem($filter);
        $this->entityManager->remove($item);
        $this->entityManager->flush();
    }

    public function updateItem($object, Request $request)
    {
        // TODO: Implement updateItem() method.
    }

    /**
     * @return string
     */
    public function getListTitle()
    {
        return $this->listTitle;
    }

    /**
     * @param string $listTitle
     */
    public function setListTitle($listTitle)
    {
        $this->listTitle = $listTitle;
    }

    public function createItem(Request $request)
    {
        // TODO: Implement createItem() method.
    }

    /**
     * @return mixed
     */
    public function initItem()
    {
        $className = $this->repository->getClassName();
        return new $className();
    }

    public function getAdditional()
    {
        return [];
    }
}