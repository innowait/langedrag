<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 21:50
 */

namespace AdminBundle\Service;


use AdminBundle\Model\AdminFilter;
use Symfony\Component\HttpFoundation\Request;

interface AdminServiceInterface
{
    public function getItem(AdminFilter $filter);
    public function getItems(AdminFilter $filter);
    public function deleteItem(AdminFilter $filter);
    public function updateItem($object,  Request $request);
    public function createItem(Request $request);
    public function initItem();
}