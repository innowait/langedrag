<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 21:50
 */

namespace AdminBundle\Model;


use Symfony\Component\HttpFoundation\Request;

class AdminFilter
{
    private $id;

    private $query;

    public function __construct(Request $request = null)
    {
        if ($request) {
            $this->query = $request->get('query');
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }
}