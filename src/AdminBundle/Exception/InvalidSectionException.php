<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 21:48
 */

namespace AdminBundle\Exception;


class InvalidSectionException extends \Exception
{
    protected $message = 'Invalid Section';
}