<?php

namespace AdminBundle\Controller;

use AdminBundle\Exception\InvalidSectionException;
use AdminBundle\Model\AdminFilter;
use AdminBundle\Service\AbstractAdminService;
use AdminBundle\Service\AdminServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="app_admin_index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_admin_dashboard');
        }
        return $this->render('@Admin/template/auth.template.html.twig');
    }

    /**
     * @Route("/dashboard", name="app_admin_dashboard")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dashboardAction()
    {
        return $this->render('@Admin/template/dashboard.template.html.twig');
    }

    /**
     * @Route("/{section}", name="app_admin_section_list")
     */
    public function sectionListAdmin($section, Request $request)
    {
        $serviceName = "app.admin.{$section}.service";
        if (!$this->has($serviceName)) {
            throw new InvalidSectionException();
        }
        /** @var AbstractAdminService $service */
        $service = $this->get($serviceName);
        $filters = new AdminFilter($request);

        $items = $service->getItems($filters);

        if ($request->isMethod(Request::METHOD_POST)) {
            $service->createItem($request);
            $this->addFlash('success', 'Item has been created!');
            return $this->redirectToRoute('app_admin_section_list', ['section' => $section]);
        }

        return $this->render($service->templates['list'], [
            'items' => $items,
            'caption' => $service->getListTitle(),
            'section' => $section
        ]);
    }

    /**
     * @Route("/{section}/add", name="app_admin_section_add")
     */
    public function sectionAddAdmin($section, Request $request)
    {
        $serviceName = "app.admin.{$section}.service";
        if (!$this->has($serviceName)) {
            throw new InvalidSectionException();
        }
        /** @var AbstractAdminService $service */
        $service = $this->get($serviceName);
        $item = $service->initItem();
        if ($request->isMethod(Request::METHOD_POST)) {
            $service->createItem($request);
            $this->addFlash('success', 'Item has been created!');
            return $this->redirectToRoute('app_admin_section_list', ['section' => $section]);
        }
        return $this->render($service->templates['edit'], [
            'item' => $item,
            'caption' => $service->getListTitle(),
            'languages' => $this->get('app.language.service')->getLanguages(),
            'section' => $section,
            'additional' => $service->getAdditional()
        ]);
    }

    /**
     * @Route("/{section}/{id}/edit", name="app_admin_section_edit")
     */
    public function sectionEditAdmin($section, $id, Request $request)
    {
        $serviceName = "app.admin.{$section}.service";
        if (!$this->has($serviceName)) {
            throw new InvalidSectionException();
        }
        /** @var AbstractAdminService $service */
        $service = $this->get($serviceName);
        $filter = (new AdminFilter())->setId($id);
        $item = $service->getItem($filter);

        if ($request->isMethod(Request::METHOD_POST)) {
            $service->updateItem($item, $request);
            $this->addFlash('success', 'Item has been updated!');
            if (isset($request->request->all()['apply'])) {
                return $this->redirectToRoute('app_admin_section_edit', ['section' => $section, 'id' => $id]);
            } else {
                return $this->redirectToRoute('app_admin_section_list', ['section' => $section]);
            }
        }
        return $this->render($service->templates['edit'], [
            'item' => $item,
            'caption' => $service->getListTitle(),
            'languages' => $this->get('app.language.service')->getLanguages(),
            'section' => $section,
            'additional' => $service->getAdditional()
        ]);
    }

    /**
     * @Route("/{section}/{id}/delete", name="app_admin_section_delete")
     */
    public function sectionDeleteAdmin($section, $id, Request $request)
    {
        $serviceName = "app.admin.{$section}.service";
        if (!$this->has($serviceName)) {
            throw new InvalidSectionException();
        }
        /** @var AbstractAdminService $service */
        $service = $this->get($serviceName);
        $filter = (new AdminFilter())->setId($id);
        $service->deleteItem($filter);
        $this->addFlash('success', 'Item has been deleted!');
        return $this->redirectToRoute('app_admin_section_list', ['section' => $section]);
    }
}
