<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 05/07/2017
 * Time: 01:40
 */

namespace CatalogBundle\Service;


use CatalogBundle\Entity\Item;
use Doctrine\ORM\EntityManager;

class CatalogService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getCatalogItem($categorySlug, $slug)
    {
        return $this->entityManager->getRepository('CatalogBundle:Item')->createQueryBuilder('item')
            ->select('item', 'languageSpecifics')
            ->leftJoin('item.languageSpecifics', 'languageSpecifics')
            ->leftJoin('item.category', 'category')
            ->leftJoin('category.languageSpecifics', 'categoryLanguage')
            ->where('languageSpecifics.slug = :slug')
            ->andWhere('categoryLanguage.slug = :categorySlug')
            ->setParameter('slug', $slug)
            ->setParameter('categorySlug', $categorySlug)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getSimilarItems(Item $item)
    {
        return $this->entityManager->getRepository('CatalogBundle:Item')->createQueryBuilder('item')
            ->select('item', 'languageSpecifics')
            ->leftJoin('item.languageSpecifics', 'languageSpecifics')
            ->leftJoin('item.category', 'category')
            ->where('item.id != :id')
            ->andWhere('category.id = :category')
            ->setParameter('id', $item->getId())
            ->setParameter('category', $item->getCategory()->getId())
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();
    }

    public function getAllCategories($full = false, $locale = false)
    {
        $query = $this->entityManager->getRepository('CatalogBundle:Category')->createQueryBuilder('category')
            ->select('category', 'languageSpecifics')
            ->leftJoin('category.languageSpecifics', 'languageSpecifics');

        if ($locale) {
            $query->andWhere('languageSpecifics.language = :language')
                ->setParameter('language', $locale);
        }
        if ($full) {
            $query->leftJoin('category.items', 'items')
                ->leftJoin('items.languageSpecifics', 'items_language_specifics')
                ->addSelect('items')
                ->addSelect('items_language_specifics');
        }
        $query->orderBy('category.order', 'ASC');
        return $query->getQuery()
            ->getResult();
    }

    public function getCatalogCategory($slug, $locale = false)
    {
         $query = $this->entityManager->getRepository('CatalogBundle:Category')->createQueryBuilder('category')
            ->select('category', 'languageSpecifics', 'items', 'items_language_specifics')
            ->leftJoin('category.languageSpecifics', 'languageSpecifics')
            ->leftJoin('category.items', 'items')
            ->leftJoin('items.languageSpecifics', 'items_language_specifics')
            ->where('languageSpecifics.slug = :slug')
            ->setParameter('slug', $slug);

        if ($locale) {
            $query->andWhere('languageSpecifics.language = :language')
                ->setParameter('language', $locale);
        }
        return $query->getQuery()
            ->getOneOrNullResult();
    }
}