<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 22:01
 */

namespace CatalogBundle\Service;


use AdminBundle\Model\AdminFilter;
use AdminBundle\Service\AbstractAdminService;
use CatalogBundle\Entity\Category;
use CatalogBundle\Entity\CategoryLanguageSpecifics;
use Doctrine\Common\Collections\ArrayCollection;
use LanguageBundle\Entity\Language;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class CategoryAdminService extends AbstractAdminService
{
    protected $listTitle = 'Category List';

    public $templates = [
        'list' => "@Catalog/admin/template/catalog_category/list.template.html.twig",
        'edit' => "@Catalog/admin/template/catalog_category/edit.template.html.twig"
    ];

    public function getAdditional()
    {
        return [
            'blocks' => $this->entityManager->getRepository('AppBundle:TemplateBlock')->findAll()
        ];
    }

    public function getItems(AdminFilter $filter)
    {
        $repo = $this->repository->createQueryBuilder('category')
            ->select('category, languageSpecifics, items, itemLanguageSpecifics')
            ->leftJoin('category.languageSpecifics', 'languageSpecifics')
            ->leftJoin('category.items', 'items')
            ->leftJoin('items.languageSpecifics', 'itemLanguageSpecifics');
        return $repo->getQuery()->getResult();
    }

    public function createItem(Request $request)
    {
        $lastOrder = $this->repository->findOneBy([], ['order' => 'DESC']);
        $languages = $this->languageService->getLanguages();
        /** @var Category $item */
        $item = $this->initItem();
        $data = $request->request->all();
        if ($lastOrder) {
            $item->setOrder($lastOrder->getOrder()+1);
        } else {
            $item->setOrder(1);
        }
        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = new CategoryLanguageSpecifics();
            $languageSpecific->setLanguage($language);
            $languageSpecific->setName($data[$language->getKey()]['name']);
            $languageSpecific->setDescription($data[$language->getKey()]['description']);
            $languageSpecific->setCategory($item);
            $this->entityManager->persist($languageSpecific);
            $item->getLanguageSpecifics()->add($languageSpecific);
        }
        $item->setVideo($data['video']);

        $blocks = $request->get('blocks', []);
        $blockArray = new ArrayCollection();
        foreach ($blocks as $blockId) {
            $blockEntity = $this->entityManager->getRepository('AppBundle:TemplateBlock')->findOneBy(['id' => $blockId]);
            $blockArray->add($blockEntity);
        }
        $item->setBlocks($blockArray);

        $image = $request->files->get('background');
        if ($image instanceof UploadedFile) {
            $item->setBackground($this->fileService->uploadFile($image, 'catalog-category'));
        }

        $this->entityManager->flush();
        return $item;
    }

    /**
     * @param Category $item
     * @param Request $request
     * @return mixed
     */
    public function updateItem($item, Request $request)
    {
        $languages = $this->languageService->getLanguages();
        $data = $request->request->all();
        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = $item->languageData($language->getKey());
            $languageSpecific->setLanguage($language);
            $languageSpecific->setName($data[$language->getKey()]['name']);
            $languageSpecific->setDescription($data[$language->getKey()]['description']);
            $languageSpecific->setCategory($item);
            $this->entityManager->persist($languageSpecific);
        }
        $item->setVideo($data['video']);

        $blocks = $request->get('blocks', []);
        $blockArray = new ArrayCollection();
        foreach ($blocks as $blockId) {
            $blockEntity = $this->entityManager->getRepository('AppBundle:TemplateBlock')->findOneBy(['id' => $blockId]);
            $blockArray->add($blockEntity);
        }
        $item->setBlocks($blockArray);

        $image = $request->files->get('background');
        if ($image instanceof UploadedFile) {
            $item->setBackground($this->fileService->uploadFile($image, 'catalog-category'));
        }

        $this->entityManager->flush();
        return $item;
    }
}