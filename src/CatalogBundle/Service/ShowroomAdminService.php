<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 22:01
 */

namespace CatalogBundle\Service;


use AdminBundle\Service\AbstractAdminService;
use CatalogBundle\Entity\Item;
use CatalogBundle\Entity\ItemLanguageSpecifics;
use CatalogBundle\Entity\Showroom;
use CatalogBundle\Entity\ShowroomLanguageSpecifics;
use LanguageBundle\Entity\Language;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class ShowroomAdminService extends AbstractAdminService
{
    protected $listTitle = 'Showroom List';

    public $templates = [
        'list' => "@Catalog/admin/template/showroom/list.template.html.twig",
        'edit' => "@Catalog/admin/template/showroom/edit.template.html.twig"
    ];

    public function getAdditional()
    {
        $items = $this->entityManager->getRepository('CatalogBundle:Item')->createQueryBuilder('item')
            ->select('item, languageSpecific')
            ->leftJoin('item.languageSpecifics', 'languageSpecific')
            ->getQuery()->getResult();
        $result = [];
        /** @var Item $item */
        foreach ($items as $item) {
            $result[] = $item->languageData($this->languageService->getCurrentLanguage()->getKey());
        }
        return [
            'items' => $result
        ];
    }

    public function createItem(Request $request)
    {
        $languages = $this->languageService->getLanguages();
        /** @var Showroom $item */
        $item = $this->initItem();
        $data = $request->request->all();

        $items = $data['items'];
        $catalogItems = $this->entityManager->getRepository('CatalogBundle:Item')->findBy(['id' => $items]);
        $item->setCatalogItems($catalogItems);
        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = new ShowroomLanguageSpecifics();
            $languageSpecific->setLanguage($language);
            $languageSpecific->setName($data[$language->getKey()]['name']);
            $languageSpecific->setDescription($data[$language->getKey()]['description']);
            $languageSpecific->setItem($item);
            $this->entityManager->persist($languageSpecific);
            $item->getLanguageSpecifics()->add($languageSpecific);
        }
        $image = $request->files->get('image');
        if ($image instanceof UploadedFile) {
            $item->setImage($this->fileService->uploadFile($image, 'showroom'));
        }

        $this->entityManager->flush();
        return $item;
    }

    /**
     * @param Showroom $item
     * @param Request $request
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateItem($item, Request $request)
    {
        $languages = $this->languageService->getLanguages();
        $data = $request->request->all();

        $items = $data['items'];
        $catalogItems = $this->entityManager->getRepository('CatalogBundle:Item')->findBy(['id' => $items]);
        $item->setCatalogItems($catalogItems);

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = $item->languageData($language->getKey());
            $languageSpecific->setLanguage($language);
            $languageSpecific->setName($data[$language->getKey()]['name']);
            $languageSpecific->setDescription($data[$language->getKey()]['description']);
            $languageSpecific->setItem($item);
            $this->entityManager->persist($languageSpecific);
        }

        $image = $request->files->get('image');
        if ($image instanceof UploadedFile) {
            $item->setImage($this->fileService->uploadFile($image, 'showroom'));
        }
        $this->entityManager->flush();
        return $item;
    }
}