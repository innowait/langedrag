<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 22:01
 */

namespace CatalogBundle\Service;


use AdminBundle\Model\AdminFilter;
use AdminBundle\Service\AbstractAdminService;
use CatalogBundle\Entity\Item;
use CatalogBundle\Entity\ItemLanguageSpecifics;
use CatalogBundle\Entity\Slide;
use CatalogBundle\Entity\SlideLanguageSpecifics;
use LanguageBundle\Entity\Language;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class SlideAdminService extends AbstractAdminService
{
    protected $listTitle = 'Slide List';

    public $templates = [
        'list' => "@Catalog/admin/template/slide/list.template.html.twig",
        'edit' => "@Catalog/admin/template/slide/edit.template.html.twig"
    ];

    public function getItems(AdminFilter $filter)
    {
        return $this->repository->createQueryBuilder('item')
            ->select('item, languageSpecific')
            ->leftJoin('item.languageSpecifics', 'languageSpecific')
            ->getQuery()->getResult();
    }

    public function getAdditional()
    {
        return [];
    }

    public function createItem(Request $request)
    {
        $languages = $this->languageService->getLanguages();
        /** @var Slide $item */
        $item = $this->initItem();
        $data = $request->request->all();

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = new SlideLanguageSpecifics();
            $languageSpecific->setLanguage($language);
            $languageSpecific->setTitle($data[$language->getKey()]['title']);
            $languageSpecific->setLink($data[$language->getKey()]['link']);
            $languageSpecific->setSlide($item);
            $this->entityManager->persist($languageSpecific);
            $item->getLanguageSpecifics()->add($languageSpecific);
        }

        $image = $request->files->get('image');
        if ($image instanceof UploadedFile) {
            $item->setImage($this->fileService->uploadFile($image, 'slide'));
        }

        $mobileImage = $request->files->get('mobile_image');
        if ($mobileImage instanceof UploadedFile) {
            $item->setMobileImage($this->fileService->uploadFile($mobileImage, 'mobile_slide'));
        }

        $this->entityManager->flush();
        return $item;
    }

    /**
     * @param Slide $item
     * @param Request $request
     * @return mixed
     */
    public function updateItem($item, Request $request)
    {
        $languages = $this->languageService->getLanguages();
        $data = $request->request->all();
        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = $item->languageData($language->getKey());
            $languageSpecific->setLanguage($language);
            $languageSpecific->setTitle($data[$language->getKey()]['title']);
            $languageSpecific->setLink($data[$language->getKey()]['link']);
            $languageSpecific->setSlide($item);
            $this->entityManager->persist($languageSpecific);
        }

        $image = $request->files->get('image');
        if ($image instanceof UploadedFile) {
            $item->setImage($this->fileService->uploadFile($image, 'slide'));
        }

        $mobileImage = $request->files->get('mobile_image');
        if ($mobileImage instanceof UploadedFile) {
            $item->setMobileImage($this->fileService->uploadFile($mobileImage, 'mobile_slide'));
        }

        $this->entityManager->flush();
        return $item;
    }
}