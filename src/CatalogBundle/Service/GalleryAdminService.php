<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 22:01
 */

namespace CatalogBundle\Service;


use AdminBundle\Model\AdminFilter;
use AdminBundle\Service\AbstractAdminService;
use CatalogBundle\Entity\GalleryImage;
use CatalogBundle\Entity\Item;
use CatalogBundle\Entity\ItemLanguageSpecifics;
use LanguageBundle\Entity\Language;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class GalleryAdminService extends AbstractAdminService
{
    protected $listTitle = 'Gallery Images List';

    public $templates = [
        'list' => "@Catalog/admin/template/gallery/list.template.html.twig",
        'edit' => "@Catalog/admin/template/gallery/edit.template.html.twig"
    ];

    public function getAdditional()
    {
        return [];
    }

    public function createItem(Request $request)
    {
        /** @var GalleryImage $item */
        $item = $this->initItem();

        $image = $request->files->get('image');
        if ($image instanceof UploadedFile) {
            $item->setImage($this->fileService->uploadFile($image, 'gallery'));
        }

        $this->entityManager->persist($item);
        $this->entityManager->flush();
        return $item;
    }
}