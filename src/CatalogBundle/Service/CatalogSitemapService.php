<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-08-01
 * Time: 22:45
 */

namespace CatalogBundle\Service;


use AppBundle\Service\AbstractSitemapService;
use AppBundle\Service\SitemapLink;
use CatalogBundle\Entity\ItemLanguageSpecifics;
use Symfony\Component\Routing\Router;

class CatalogSitemapService extends AbstractSitemapService
{
    public function getLinks()
    {
//        $products = $this->entityManager->getRepository('CatalogBundle:ItemLanguageSpecifics')->findAll();
//        /** @var ItemLanguageSpecifics $product */
//        foreach ($products as $product) {
//            $link = new SitemapLink();
//            $link->setUrl($this->router->generate(
//                'app_catalog_item_single',
//                [
//                    'category' => $product->getItem()->getCategory()->languageData($product->getLanguage()->getKey())->getSlug(),
//                    'slug' => $product->getSlug(),
//                    '_locale' => $product->getLanguage()->getKey()
//                ],
//                Router::ABSOLUTE_URL
//            ));
//            $links[] = $link;
//        }
//
//
//        $showrooms = $this->entityManager->getRepository('CatalogBundle:ShowroomLanguageSpecifics')->findAll();
//        foreach ($showrooms as $showroom) {
//            $link = new SitemapLink();
//            $link->setUrl($this->router->generate(
//                'app_catalog_showroom_single',
//                [
//                    'slug' => $showroom->getSlug(),
//                    '_locale' => $showroom->getLanguage()->getKey()
//                ],
//                Router::ABSOLUTE_URL
//            ));
//            $links[] = $link;
//        }
//        return $links;
    }
}