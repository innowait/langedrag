<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 22:01
 */

namespace CatalogBundle\Service;


use AdminBundle\Model\AdminFilter;
use AdminBundle\Service\AbstractAdminService;
use CatalogBundle\Entity\Item;
use CatalogBundle\Entity\ItemLanguageSpecifics;
use CatalogBundle\Entity\ProductSpecialBlock;
use LanguageBundle\Entity\Language;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class ProductSpecialBlockAdminService extends AbstractAdminService
{
    protected $listTitle = 'Item List';

    public $templates = [
        'list' => "@Catalog/admin/template/special_block/list.template.html.twig",
        'edit' => "@Catalog/admin/template/special_block/edit.template.html.twig"
    ];

    public function getItems(AdminFilter $filter)
    {
        return $this->repository->createQueryBuilder('item')
            ->select('item')
            ->getQuery()
            ->getResult();
    }

    public function getAdditional()
    {
        return [];
    }

    public function createItem(Request $request)
    {
        /** @var ProductSpecialBlock $item */
        $item = $this->initItem();
        $data = $request->request->all();

        $item->setTitle($data['title']);
        $item->setDescription($data['description']);

        $this->entityManager->persist($item);
        $this->entityManager->flush();
        return $item;
    }

    /**
     * @param ProductSpecialBlock $item
     * @param Request $request
     *
     * @return ProductSpecialBlock
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateItem($item, Request $request)
    {
        $data = $request->request->all();

        $item->setTitle($data['title']);
        $item->setDescription($data['description']);

        $this->entityManager->flush();
        return $item;
    }
}