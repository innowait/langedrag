<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 22:01
 */

namespace CatalogBundle\Service;


use AdminBundle\Model\AdminFilter;
use AdminBundle\Service\AbstractAdminService;
use CatalogBundle\Entity\Item;
use CatalogBundle\Entity\ItemLanguageSpecifics;
use LanguageBundle\Entity\Language;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class ItemAdminService extends AbstractAdminService
{
    protected $listTitle = 'Item List';

    public $templates = [
        'list' => "@Catalog/admin/template/catalog_item/list.template.html.twig",
        'edit' => "@Catalog/admin/template/catalog_item/edit.template.html.twig"
    ];

    public function getItems(AdminFilter $filter)
    {
        return $this->repository->createQueryBuilder('item')
            ->select('item, languageSpecific')
            ->leftJoin('item.languageSpecifics', 'languageSpecific')
            ->getQuery()->getResult();
    }

    public function getAdditional()
    {
        return [];
    }

    public function createItem(Request $request)
    {
        $languages = $this->languageService->getLanguages();
        /** @var Item $item */
        $item = $this->initItem();
        $data = $request->request->all();

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = new ItemLanguageSpecifics();
            $languageSpecific->setLanguage($language);
            $languageSpecific->setName($data[$language->getKey()]['name']);
            $languageSpecific->setDescription($data[$language->getKey()]['description']);
            $languageSpecific->setItem($item);
            $this->entityManager->persist($languageSpecific);
            $item->getLanguageSpecifics()->add($languageSpecific);
        }

        $image = $request->files->get('image');
        if ($image instanceof UploadedFile) {
            $item->setImage($this->fileService->uploadFile($image, 'catalog-item'));
        }

        $this->entityManager->flush();
        return $item;
    }

    /**
     * @param Item $item
     * @param Request $request
     * @return mixed
     */
    public function updateItem($item, Request $request)
    {
        $languages = $this->languageService->getLanguages();
        $data = $request->request->all();
        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = $item->languageData($language->getKey());
            $languageSpecific->setLanguage($language);
            $languageSpecific->setName($data[$language->getKey()]['name']);
            $languageSpecific->setDescription($data[$language->getKey()]['description']);
            $languageSpecific->setItem($item);
            $this->entityManager->persist($languageSpecific);
        }

        $image = $request->files->get('image');
        if ($image instanceof UploadedFile) {
            $item->setImage($this->fileService->uploadFile($image, 'catalog-item'));
        }

        $this->entityManager->flush();
        return $item;
    }
}