<?php

namespace CatalogBundle\Controller;

use CatalogBundle\Entity\ShowroomLanguageSpecifics;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/{slug}", name="app_catalog_category_single")
     *
     * @param $slug
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function singleCategoryAction($slug, Request $request)
    {
        $item = $this->get('app.catalog.service')->getCatalogCategory($slug, $this->get('app.language.service')->getCurrentLanguage());
        if (!$item) {
            $item = $this->get('app.catalog.service')->getCatalogCategory($slug);
            if ($item) {
                $this->getDoctrine()->getManager()->refresh($item);
                $correctSlug = $item->languageData($request->getLocale())->getSlug();
                return $this->redirectToRoute('app_catalog_category_single', ['slug' => $correctSlug]);
            } else {
                throw new NotFoundHttpException('Category was not found', null, 404);
            }
        }
        return $this->render('@Catalog/template/single.category.template.html.twig', [
            'category' => $item
        ]);
    }

    /**
     * @Route("/showroom/{slug}", name="app_catalog_showroom_single")
     *
     * @param $slug
     * @param Request $request
     */
    public function showroomAction($slug, Request $request)
    {
        $languageService = $this->get('app.language.service');

        /** @var ShowroomLanguageSpecifics $item */
        $item = $this->getDoctrine()->getRepository('CatalogBundle:ShowroomLanguageSpecifics')->findOneBy(['slug' => $slug]);
        if ($item->getLanguage() !== $languageService->getCurrentLanguage()) {
            $redirectItem = $item->getItem()->languageData($languageService->getCurrentLanguage()->getKey());
            if (!$redirectItem) {
                throw new NotFoundHttpException('Showroom not found');
            }
            return $this->redirectToRoute('app_catalog_showroom_single', ['slug' => $redirectItem->getSlug()]);
        }

        return $this->render('@Catalog/template/single.showroom.template.html.twig', [
            'showroom' => $item->getItem()
        ]);
    }

    /**
     * @Route("/{category}/{slug}", name="app_catalog_item_single")
     * @return mixed
     */
    public function singleAction($category, $slug, Request $request)
    {
        $item = $this->get('app.catalog.service')->getCatalogItem($category, $slug);
        if (!$item) {
            throw new NotFoundHttpException('Product was not found', null, 404);
        }
        $similar = $this->get('app.catalog.service')->getSimilarItems($item);
        return $this->render('@Catalog/template/single.product.template.html.twig', [
            'product' => $item,
            'similar' => $similar
        ]);
    }
}
