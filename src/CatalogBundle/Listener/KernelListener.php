<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 06/12/2017
 * Time: 16:57
 */

namespace CatalogBundle\Listener;


use CatalogBundle\Service\CatalogService;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class KernelListener
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var CatalogService
     */
    private $catalogService;

    public function __construct(\Twig_Environment $twig, CatalogService $catalogService)
    {
        $this->twig = $twig;
        $this->catalogService = $catalogService;
    }

    /**
     * On Kernel Request
     *
     * @param $e
     */
    public function onKernelRequest(GetResponseEvent $e)
    {
        $categories = $this->catalogService->getAllCategories();
        $this->twig->addGlobal('availableCategories', $categories);
    }
}