<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 15:53
 */

namespace CatalogBundle\Entity;


use AppBundle\Entity\Traits\IdEntity;
use AppBundle\Entity\Traits\LanguageSpecific;
use AppBundle\Entity\Traits\Sluggable;
use AppBundle\Entity\Traits\SluggableLanguageSpecific;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ItemLanguageSpecifics
 * @package CatalogBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class ItemLanguageSpecifics
{
    use IdEntity;
    use SluggableLanguageSpecific;
    use LanguageSpecific;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="CatalogBundle\Entity\Item", inversedBy="languageSpecifics", cascade={"persist"})
     */
    private $item;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->slug = (new Slugify())->slugify($this->name);
        if ($this->name == '') {
            $this->name = '';
            $this->name .= $this->getItem()->getCategory()->languageData($this->getLanguage()->getKey())->getName() . ' ';
            $this->name .= mt_rand(1000,9999);
        }
    }
}