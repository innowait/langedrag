<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-04-16
 * Time: 17:21
 */

namespace CatalogBundle\Entity;

use AppBundle\Entity\Traits\IdEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Showroom
 * @package CatalogBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class Showroom
{
    use IdEntity;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $image;

    /**
     * @var Item[]
     *
     * @ORM\ManyToMany(targetEntity="CatalogBundle\Entity\Item")
     */
    private $catalogItems;

    /**
     * @var ArrayCollection|ShowroomLanguageSpecifics[]
     *
     * @ORM\OneToMany(targetEntity="CatalogBundle\Entity\ShowroomLanguageSpecifics", mappedBy="item", fetch="EAGER", cascade={"persist"}, orphanRemoval=true)
     */
    private $languageSpecifics;

    public function __construct()
    {
        $this->languageSpecifics = new ArrayCollection();
        $this->catalogItems = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return Item[]
     */
    public function getCatalogItems()
    {
        return $this->catalogItems;
    }

    /**
     * @param Item[] $catalogItems
     */
    public function setCatalogItems($catalogItems)
    {
        $this->catalogItems = $catalogItems;
    }

    /**
     * @return ItemLanguageSpecifics|ArrayCollection
     */
    public function getLanguageSpecifics()
    {
        return $this->languageSpecifics;
    }

    /**
     * @param ItemLanguageSpecifics|ArrayCollection $languageSpecifics
     */
    public function setLanguageSpecifics($languageSpecifics)
    {
        $this->languageSpecifics = $languageSpecifics;
    }

    /**
     * @param $locale
     * @return ShowroomLanguageSpecifics|mixed
     */
    public function languageData($locale)
    {
        foreach ($this->languageSpecifics as $languageSpecific) {
            if ($languageSpecific->getLanguage()->getKey() == $locale) {
                return $languageSpecific;
            }
        }
        return new ShowroomLanguageSpecifics();
    }

    /**
     * @param Item $item
     * @return bool
     */
    public function hasItem(Item $item)
    {
        foreach ($this->catalogItems as $catalogItem) {
            if ($catalogItem->getId() == $item->getId()) {
                return true;
            }
        }
        return false;
    }
}