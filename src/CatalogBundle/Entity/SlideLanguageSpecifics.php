<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-11-22
 * Time: 09:39
 */

namespace CatalogBundle\Entity;


use AppBundle\Entity\Traits\IdEntity;
use AppBundle\Entity\Traits\LanguageSpecific;
use Doctrine\ORM\Mapping as ORM;
use LanguageBundle\Entity\Language;

/**
 * Class SlideLanguageSpecifics
 * @package CatalogBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class SlideLanguageSpecifics
{
    use IdEntity;
    use LanguageSpecific;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $link;


    /**
     * @var Slide
     *
     * @ORM\ManyToOne(targetEntity="CatalogBundle\Entity\Slide", inversedBy="languageSpecifics", cascade={"persist"})
     */
    private $slide;

    /**
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param Language $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return Slide
     */
    public function getSlide()
    {
        return $this->slide;
    }

    /**
     * @param Slide $slide
     */
    public function setSlide($slide)
    {
        $this->slide = $slide;
    }
}