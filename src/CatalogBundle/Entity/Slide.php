<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-11-22
 * Time: 09:38
 */

namespace CatalogBundle\Entity;

use AppBundle\Entity\Traits\IdEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Slide
 * @package CatalogBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class Slide
{
    use IdEntity;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $mobileImage;

    /**
     * @var ArrayCollection|ItemLanguageSpecifics
     *
     * @ORM\OneToMany(targetEntity="CatalogBundle\Entity\SlideLanguageSpecifics", mappedBy="slide", fetch="EAGER", cascade={"persist"}, orphanRemoval=true)
     */
    private $languageSpecifics;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordering", type="integer", nullable=true)
     */
    private $order;

    public function __construct()
    {
        $this->languageSpecifics = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return ItemLanguageSpecifics|ArrayCollection
     */
    public function getLanguageSpecifics()
    {
        return $this->languageSpecifics;
    }

    /**
     * @param ItemLanguageSpecifics|ArrayCollection $languageSpecifics
     */
    public function setLanguageSpecifics($languageSpecifics)
    {
        $this->languageSpecifics = $languageSpecifics;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getMobileImage()
    {
        return $this->mobileImage;
    }

    /**
     * @param string $mobileImage
     */
    public function setMobileImage($mobileImage)
    {
        $this->mobileImage = $mobileImage;
    }

    public function languageData($locale)
    {
        foreach ($this->languageSpecifics as $languageSpecific) {
            if ($languageSpecific->getLanguage()->getKey() == $locale) {
                return $languageSpecific;
            }
        }
        return new SlideLanguageSpecifics();
    }

    public function webP($mobile = false) {
        if ($mobile) {
            return str_replace('.jpg', '.webp', $this->getMobileImage());
        } else {
            return str_replace('.jpg', '.webp', $this->getImage());
        }
    }
}