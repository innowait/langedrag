<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-10-21
 * Time: 22:29
 */

namespace CatalogBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class ProductSpecialBlock
 * @package CatalogBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table()
 */
class ProductSpecialBlock
{
    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $description;

    /**
     * @var Item[]
     *
     * @ORM\ManyToMany(targetEntity="CatalogBundle\Entity\Item", mappedBy="specialBlocks")
     */
    private $items;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Item[] $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }
}