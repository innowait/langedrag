<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 15:45
 */

namespace CatalogBundle\Entity;

use AppBundle\Entity\Traits\IdEntity;
use AppBundle\Entity\Traits\Sluggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Item
 * @package CatalogBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class Item
{
    use IdEntity;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="CatalogBundle\Entity\Category", inversedBy="items")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    /**
     * @var ArrayCollection|ItemLanguageSpecifics
     *
     * @ORM\OneToMany(targetEntity="CatalogBundle\Entity\ItemLanguageSpecifics", mappedBy="item", fetch="EAGER", cascade={"persist"}, orphanRemoval=true)
     */
    private $languageSpecifics;

    /**
     * Item constructor.
     */
    public function __construct()
    {
        $this->languageSpecifics = new ArrayCollection();
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return ItemLanguageSpecifics|ArrayCollection
     */
    public function getLanguageSpecifics()
    {
        return $this->languageSpecifics;
    }

    /**
     * @param ItemLanguageSpecifics|ArrayCollection $languageSpecifics
     */
    public function setLanguageSpecifics($languageSpecifics)
    {
        $this->languageSpecifics = $languageSpecifics;
    }

    public function languageData($locale)
    {
        foreach ($this->languageSpecifics as $languageSpecific) {
            if ($languageSpecific->getLanguage()->getKey() == $locale) {
                return $languageSpecific;
            }
        }
        return new ItemLanguageSpecifics();
    }

    public function webP()
    {
        return str_replace('.jpg', '.webp', $this->getImage());
    }
}