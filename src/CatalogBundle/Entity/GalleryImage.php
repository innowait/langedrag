<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-11-22
 * Time: 11:28
 */

namespace CatalogBundle\Entity;

use AppBundle\Entity\Traits\IdEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class GalleryImage
 * @package CatalogBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class GalleryImage
{
    use IdEntity;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $image;

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}