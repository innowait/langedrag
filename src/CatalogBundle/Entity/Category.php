<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 15:45
 */

namespace CatalogBundle\Entity;

use AppBundle\Entity\TemplateBlock;
use AppBundle\Entity\Traits\IdEntity;
use AppBundle\Entity\Traits\Sluggable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Category
 * @package CatalogBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class Category
{
    use IdEntity;

    /**
     * @var ArrayCollection|CategoryLanguageSpecifics[]
     *
     * @ORM\OneToMany(targetEntity="CatalogBundle\Entity\CategoryLanguageSpecifics", mappedBy="category", orphanRemoval=true, fetch="EAGER")
     */
    private $languageSpecifics;

    /**
     * @var Item[]
     *
     * @ORM\OneToMany(targetEntity="CatalogBundle\Entity\Item", mappedBy="category")
     */
    private $items;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $background;

    /**
     * @var TemplateBlock[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\TemplateBlock", inversedBy="categories")
     */
    private $blocks;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $video;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", unique=true, name="sort_order")
     */
    private $order;

    public function __construct()
    {
        $this->languageSpecifics = new ArrayCollection();
        $this->blocks = new ArrayCollection();
    }

    /**
     * @return CategoryLanguageSpecifics|ArrayCollection
     */
    public function getLanguageSpecifics()
    {
        return $this->languageSpecifics;
    }

    /**
     * @param CategoryLanguageSpecifics|ArrayCollection $languageSpecifics
     */
    public function setLanguageSpecifics($languageSpecifics)
    {
        $this->languageSpecifics = $languageSpecifics;
    }

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return Item[]
     */
    public function getFeaturedItems()
    {
        $result = [];
        foreach ($this->items as $item) {
            if ($item->isFeatured()) {
                $result[] = $item;
            }
        }
        return $result;
    }

    /**
     * @param Item[] $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return string
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * @param string $background
     */
    public function setBackground($background)
    {
        $this->background = $background;
    }

    /**
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * @param string $video
     */
    public function setVideo($video)
    {
        $this->video = $video;
    }

    /**
     * @return \AppBundle\Entity\TemplateBlock[]
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * @param \AppBundle\Entity\TemplateBlock[]|ArrayCollection $blocks
     */
    public function setBlocks($blocks)
    {
        $this->blocks = $blocks;
    }

    /**
     * @return ArrayCollection
     */
    public function getFeaturedImages()
    {
        $result = new ArrayCollection();
        foreach ($this->items as $item) {
            if ($item->isFeatured()) {
                $result->add($item);
            }
        }
        return $result;
    }

    public function languageData($locale)
    {
        foreach ($this->languageSpecifics as $languageSpecific) {
            if ($languageSpecific->getLanguage()->getKey() == $locale) {
                return $languageSpecific;
            }
        }
        return new CategoryLanguageSpecifics();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }
}