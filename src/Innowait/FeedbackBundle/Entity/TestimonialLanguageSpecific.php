<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-04-03
 * Time: 11:19
 */

namespace Innowait\FeedbackBundle\Entity;


use AppBundle\Entity\Traits\IdEntity;
use AppBundle\Entity\Traits\LanguageSpecific;
use Doctrine\ORM\Mapping as ORM;
use LanguageBundle\Entity\Language;

/**
 * Class TestimonialLanguageSpecific
 * @package Innowait\FeedbackBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class TestimonialLanguageSpecific
{
    use IdEntity;
    use LanguageSpecific;

    /**
     * @var Testimonial
     *
     * @ORM\ManyToOne(targetEntity="Innowait\FeedbackBundle\Entity\Testimonial", inversedBy="languageSpecifics")
     */
    private $testimonial;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $role;

    /**
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param Language $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return Testimonial
     */
    public function getTestimonial()
    {
        return $this->testimonial;
    }

    /**
     * @param Testimonial $testimonial
     */
    public function setTestimonial($testimonial)
    {
        $this->testimonial = $testimonial;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }
}