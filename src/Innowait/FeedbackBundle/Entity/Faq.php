<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-04-03
 * Time: 11:19
 */

namespace Innowait\FeedbackBundle\Entity;

use AppBundle\Entity\Traits\IdEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Testimonial
 * @package Innowait\FeedbackBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class Faq
{
    use IdEntity;

    /**
     * @var ArrayCollection|FaqLanguageSpecific
     *
     * @ORM\OneToMany(targetEntity="Innowait\FeedbackBundle\Entity\FaqLanguageSpecific", mappedBy="faq", cascade={"persist"}, orphanRemoval=true, fetch="EAGER")
     */
    private $languageSpecifics;

    public function __construct()
    {
        $this->languageSpecifics = new ArrayCollection();
    }

    /**
     * @return ArrayCollection|TestimonialLanguageSpecific
     */
    public function getLanguageSpecifics()
    {
        return $this->languageSpecifics;
    }

    /**
     * @param ArrayCollection|FaqLanguageSpecific $languageSpecifics
     */
    public function setLanguageSpecifics($languageSpecifics)
    {
        $this->languageSpecifics = $languageSpecifics;
    }

    /**
     * @param $language
     * @return FaqLanguageSpecific
     */
    public function languageData($language)
    {
        /** @var FaqLanguageSpecific $languageSpecific */
        foreach ($this->languageSpecifics as $languageSpecific) {
            if ($languageSpecific->getLanguage()->getKey() == $language) {
                return $languageSpecific;
            }
        }
        return new FaqLanguageSpecific();
    }
}