<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-04-03
 * Time: 11:19
 */

namespace Innowait\FeedbackBundle\Entity;

use AppBundle\Entity\Traits\IdEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Testimonial
 * @package Innowait\FeedbackBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class Testimonial
{
    use IdEntity;

    /**
     * @var ArrayCollection|TestimonialLanguageSpecific
     *
     * @ORM\OneToMany(targetEntity="Innowait\FeedbackBundle\Entity\TestimonialLanguageSpecific", mappedBy="testimonial", cascade={"persist"}, orphanRemoval=true, fetch="EAGER")
     */
    private $languageSpecifics;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(type="integer")
     */
    private $rating;

    public function __construct()
    {
        $this->languageSpecifics = new ArrayCollection();
    }

    /**
     * @return ArrayCollection|TestimonialLanguageSpecific
     */
    public function getLanguageSpecifics()
    {
        return $this->languageSpecifics;
    }

    /**
     * @param ArrayCollection|TestimonialLanguageSpecific $languageSpecifics
     */
    public function setLanguageSpecifics($languageSpecifics)
    {
        $this->languageSpecifics = $languageSpecifics;
    }

    /**
     * @param $language
     * @return TestimonialLanguageSpecific
     */
    public function languageData($language)
    {
        /** @var TestimonialLanguageSpecific $languageSpecific */
        foreach ($this->languageSpecifics as $languageSpecific) {
            if ($languageSpecific->getLanguage()->getKey() == $language) {
                return $languageSpecific;
            }
        }
        return new TestimonialLanguageSpecific();
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param string $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    public function webP()
    {
        return str_replace('.png', '.webp', $this->getImage());
    }
}