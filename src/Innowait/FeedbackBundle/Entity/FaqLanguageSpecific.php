<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-04-03
 * Time: 11:19
 */

namespace Innowait\FeedbackBundle\Entity;


use AppBundle\Entity\Traits\IdEntity;
use AppBundle\Entity\Traits\LanguageSpecific;
use Doctrine\ORM\Mapping as ORM;
use LanguageBundle\Entity\Language;

/**
 * Class TestimonialLanguageSpecific
 * @package Innowait\FeedbackBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class FaqLanguageSpecific
{
    use IdEntity;
    use LanguageSpecific;

    /**
     * @var Testimonial
     *
     * @ORM\ManyToOne(targetEntity="Innowait\FeedbackBundle\Entity\Faq", inversedBy="languageSpecifics")
     */
    private $faq;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $question;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $answer;

    /**
     * @return Testimonial
     */
    public function getFaq()
    {
        return $this->faq;
    }

    /**
     * @param Faq $faq
     */
    public function setFaq($faq)
    {
        $this->faq = $faq;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }
}