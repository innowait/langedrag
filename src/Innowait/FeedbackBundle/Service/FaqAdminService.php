<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-04-03
 * Time: 11:38
 */

namespace Innowait\FeedbackBundle\Service;


use AdminBundle\Service\AbstractAdminService;
use Innowait\FeedbackBundle\Entity\Faq;
use Innowait\FeedbackBundle\Entity\FaqLanguageSpecific;
use Innowait\FeedbackBundle\Entity\Testimonial;
use Innowait\FeedbackBundle\Entity\TestimonialLanguageSpecific;
use LanguageBundle\Entity\Language;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class FaqAdminService extends AbstractAdminService
{
    protected $listTitle = 'FAQ List';

    public $templates = [
        'list' => "@InnowaitFeedback/admin/template/faq/list.template.html.twig",
        'edit' => "@InnowaitFeedback/admin/template/faq/edit.template.html.twig"
    ];

    /**
     * @param Request $request
     * @return Testimonial
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createItem(Request $request)
    {
        $languages = $this->languageService->getLanguages();
        /** @var Testimonial $item */
        $item = $this->initItem();
        $data = $request->request->all();

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = new FaqLanguageSpecific();
            $languageSpecific->setLanguage($language);
            $languageSpecific->setQuestion($data[$language->getKey()]['question']);
            $languageSpecific->setAnswer($data[$language->getKey()]['answer']);
            $languageSpecific->setFaq($item);
            $this->entityManager->persist($languageSpecific);
            $item->getLanguageSpecifics()->add($languageSpecific);
        }

        $this->entityManager->persist($item);
        $this->entityManager->flush();
        return $item;
    }

    /**
     * @param Faq $item
     * @param Request $request
     *
     * @return Faq
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateItem($item, Request $request)
    {
        $languages = $this->languageService->getLanguages();
        $data = $request->request->all();

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = $item->languageData($language->getKey());
            $languageSpecific->setLanguage($language);
            $languageSpecific->setQuestion($data[$language->getKey()]['question']);
            $languageSpecific->setAnswer($data[$language->getKey()]['answer']);
            $languageSpecific->setFaq($item);
            $this->entityManager->persist($languageSpecific);
        }

        $this->entityManager->flush();
        return $item;
    }
}