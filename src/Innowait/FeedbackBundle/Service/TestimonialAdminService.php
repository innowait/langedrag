<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-04-03
 * Time: 11:38
 */

namespace Innowait\FeedbackBundle\Service;


use AdminBundle\Service\AbstractAdminService;
use Innowait\FeedbackBundle\Entity\Testimonial;
use Innowait\FeedbackBundle\Entity\TestimonialLanguageSpecific;
use LanguageBundle\Entity\Language;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class TestimonialAdminService extends AbstractAdminService
{
    protected $listTitle = 'Testimonials List';

    public $templates = [
        'list' => "@InnowaitFeedback/admin/template/testimonial/list.template.html.twig",
        'edit' => "@InnowaitFeedback/admin/template/testimonial/edit.template.html.twig"
    ];

    /**
     * @param Request $request
     * @return Testimonial
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createItem(Request $request)
    {
        $languages = $this->languageService->getLanguages();
        /** @var Testimonial $item */
        $item = $this->initItem();
        $data = $request->request->all();

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = new TestimonialLanguageSpecific();
            $languageSpecific->setLanguage($language);
            $languageSpecific->setContent($data[$language->getKey()]['content']);
            $languageSpecific->setAuthor($data[$language->getKey()]['author']);
            $languageSpecific->setRole($data[$language->getKey()]['role']);
            $languageSpecific->setTestimonial($item);
            $this->entityManager->persist($languageSpecific);
            $item->getLanguageSpecifics()->add($languageSpecific);
        }

        $item->setRating($data['rating']);

        if ($request->files->get('image') instanceof UploadedFile) {
            $name = $this->fileService->uploadFile($request->files->get('image'), 'testimonial');
            $item->setImage($name);
        }

        $this->entityManager->persist($item);

        $this->entityManager->flush();
        return $item;
    }

    /**
     * @param Testimonial $item
     * @param Request $request
     *
     * @return Testimonial
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateItem($item, Request $request)
    {
        $languages = $this->languageService->getLanguages();
        $data = $request->request->all();

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = $item->languageData($language->getKey());
            $languageSpecific->setLanguage($language);
            $languageSpecific->setContent($data[$language->getKey()]['content']);
            $languageSpecific->setAuthor($data[$language->getKey()]['author']);
            $languageSpecific->setRole($data[$language->getKey()]['role']);
            $languageSpecific->setTestimonial($item);
            $this->entityManager->persist($languageSpecific);
        }

        $item->setRating($data['rating']);

        if ($request->files->get('image') instanceof UploadedFile) {
            $name = $this->fileService->uploadFile($request->files->get('image'), 'testimonial');
            $item->setImage($name);
            $this->entityManager->persist($item);
        }

        $this->entityManager->flush();
        return $item;
    }
}