<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-04-24
 * Time: 00:36
 */

namespace Innowait\OrderBundle\Entity;

use AppBundle\Entity\Traits\IdEntity;
use CatalogBundle\Entity\Item;
use Doctrine\ORM\Mapping as ORM;
use LanguageBundle\Entity\Language;

/**
 * Class Order
 * @package Innowait\OrderBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="orders")
 */
class Order
{
    use IdEntity;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="LanguageBundle\Entity\Language")
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $fullName;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="CatalogBundle\Entity\Item")
     */
    private $catalogItem;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $pricePerUnit;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $totalPrice;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return Item
     */
    public function getCatalogItem()
    {
        return $this->catalogItem;
    }

    /**
     * @param Item $catalogItem
     */
    public function setCatalogItem($catalogItem)
    {
        $this->catalogItem = $catalogItem;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getPricePerUnit()
    {
        return $this->pricePerUnit;
    }

    /**
     * @param float $pricePerUnit
     */
    public function setPricePerUnit($pricePerUnit)
    {
        $this->pricePerUnit = $pricePerUnit;
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param float $totalPrice
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param Language $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }
}