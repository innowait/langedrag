<?php

namespace Innowait\OrderBundle\Controller;

use Innowait\OrderBundle\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/create", name="app_order_create", options={"expose": true})
     */
    public function indexAction(Request $request)
    {
        $order = new Order();
        $itemId = $request->get('product');
        $product = $this->getDoctrine()->getRepository('CatalogBundle:Item')->findOneBy(['id' => $itemId]);
        $order->setEmail($request->get('email'));
        $order->setPhone($request->get('phone'));
        $order->setFullName($request->get('fullname'));
        $order->setLanguage($this->get('app.language.service')->getCurrentLanguage());

        $order->setCatalogItem($product);
        $order->setPricePerUnit($product->getPrice());
        $order->setQuantity($request->get('quantity'));
        $order->setTotalPrice($request->get('quantity') * $product->getPrice());

        $this->getDoctrine()->getManager()->persist($order);
        $this->getDoctrine()->getManager()->flush();

        try {
            $message = new \Swift_Message(
                'LiquiDec - New Order from Website',
                $this->renderView(':email:order.notification.email.html.twig', [
                    'order' => $order,
                    'product' => $product
                ]),
                'text/html',
                'UTF-8'
            );
            $message->setTo([
                'info@liquidec.co.uk'
            ]);
            $message->setFrom('hello@innowait.solutions');
            $this->get('mailer')->send($message);
        } catch (\Exception $e) {
        }

        return new JsonResponse([
            'success' => true,
            'order' => $order->getId()
        ]);
    }
}
