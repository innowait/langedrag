<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 25/12/2018
 * Time: 18:38
 */

namespace Innowait\HealthBundle\Service;


use Buzz\Browser;

class EventLogManager
{
    const HOST_EVENTS_URL = 'https://customer.innowait.solutions/api/events';

    const EVENT_ERROR = 'app.error';

    /**
     * @var Browser
     */
    private $buzz;

    /**
     * @var string
     */
    private $application;

    /**
     * @var string
     */
    private $env;

    public function __construct(Browser $browser)
    {
        $this->buzz = $browser;
        $this->buzz->getClient()->setTimeout(1);
    }

    /**
     * @param string $application
     */
    public function setApplication($application)
    {
        $this->application = $application;
    }

    /**
     * @param string $env
     */
    public function setEnv($env)
    {
        $this->env = $env;
    }

    /**
     * Log
     *
     * @param $event
     * @param $message
     * @param bool $value
     * @return bool
     */
    public function log($event, $message, $value = false)
    {
        $data = [
            'application' => $this->application,
            'event' => $event,
            'message' => $message,
            'environment' => $this->env
        ];

        if ($value) {
            $data['value'] = $value;
        }

        try {
            $this->buzz->post(
                self::HOST_EVENTS_URL,
                ['Content-Type' => 'application/json'],
                json_encode($data)
            );
        } catch (\Exception $exception) {
            return true;
        }
        return true;
    }
}