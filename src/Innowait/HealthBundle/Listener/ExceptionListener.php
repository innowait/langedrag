<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 25/12/2018
 * Time: 19:01
 */

namespace Innowait\HealthBundle\Listener;


use Innowait\HealthBundle\Service\EventLogManager;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Router;

class ExceptionListener
{
    /**
     * @var EventLogManager
     */
    private $eventManager;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var string
     */
    private $env;

    /**
     * @var bool
     *
     * Do you want to show trace on exceptions in DEV environment
     */
    private $traceDev = true;

    /**
     * @var Router
     */
    private $router;

    /**
     * ExceptionListener constructor.
     * @param EventLogManager $eventManager
     * @param LoggerInterface $logger
     * @param $env
     */
    public function __construct(EventLogManager $eventManager, LoggerInterface $logger, $env, Router $router)
    {
        $this->logger = $logger;
        $this->eventManager = $eventManager;
        $this->env = $env;
        $this->router = $router;
    }

    /**
     * @param $e
     */
    public function onException(GetResponseForExceptionEvent $e)
    {
        $exception = $e->getException();
        $this->logger->error($exception->getMessage());
        $this->eventManager->log(EventLogManager::EVENT_ERROR, $exception->getMessage());
        $e->stopPropagation();

        if ($this->env == 'dev' && $this->traceDev) {
            return;
        }
        $code = 500;
        if ($exception instanceof NotFoundHttpException) {
            $code = 404;
        }

        $e->setResponse(new RedirectResponse($this->router->generate('app_error_page', ['code' => $code]), 301));

        return $e;
    }
}