<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 20/08/2018
 * Time: 16:33
 */

namespace MenuBundle\Entity;

use AppBundle\Entity\Traits\IdEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Menu
 * @package MenuBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class Menu
{
    use IdEntity;

    /**
     * @var ArrayCollection|MenuLanguageSpecifics
     *
     * @ORM\OneToMany(targetEntity="MenuBundle\Entity\MenuLanguageSpecifics", mappedBy="menu", fetch="EAGER", cascade={"persist"}, orphanRemoval=true)
     */
    private $languageSpecifics;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $hash;

    public function __construct()
    {
        $this->languageSpecifics = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->hash = md5(time());
    }

    /**
     * @return ArrayCollection|MenuLanguageSpecifics
     */
    public function getLanguageSpecifics()
    {
        return $this->languageSpecifics;
    }

    /**
     * @param ArrayCollection|MenuLanguageSpecifics $languageSpecifics
     */
    public function setLanguageSpecifics($languageSpecifics)
    {
        $this->languageSpecifics = $languageSpecifics;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    public function languageData($locale)
    {
        foreach ($this->languageSpecifics as $languageSpecific) {
            if ($languageSpecific->getLanguage()->getKey() == $locale) {
                return $languageSpecific;
            }
        }
        return new MenuLanguageSpecifics();
    }
}