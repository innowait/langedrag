<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 22:01
 */

namespace MenuBundle\Service;


use AdminBundle\Service\AbstractAdminService;
use LanguageBundle\Entity\Language;
use MenuBundle\Entity\Menu;
use MenuBundle\Entity\MenuLanguageSpecifics;
use Symfony\Component\HttpFoundation\Request;

class MenuAdminService extends AbstractAdminService
{
    protected $listTitle = 'Menu List';

    public $templates = [
        'list' => "@Menu/admin/template/menu/list.template.html.twig",
        'edit' => "@Menu/admin/template/menu/edit.template.html.twig"
    ];

    public function getAdditional()
    {
        $pages = $this->entityManager
            ->getRepository('StaticPageBundle:StaticPageLanguageSpecific')
            ->findBy(['language' => $this->languageService->getCurrentLanguage()]);
        return [
            'pages' => $pages
        ];
    }

    public function createItem(Request $request)
    {
        $languages = $this->languageService->getLanguages();
        /** @var Menu $item */
        $item = $this->initItem();
        $data = $request->request->all();

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = new MenuLanguageSpecifics();
            $languageSpecific->setLanguage($language);
            $languageSpecific->setName($data[$language->getKey()]['name']);
            $languageSpecific->setMenu($item);
            $links = $data[$language->getKey()]['items'];
            foreach ($links as $key => $link) {
                if ($link['page'] == '') {
                    unset($links[$key]);
                }
            }
            $languageSpecific->setLinks($links);
            $this->entityManager->persist($languageSpecific);
            $item->getLanguageSpecifics()->add($languageSpecific);
        }

        $this->entityManager->flush();
        return $item;
    }

    /**
     * @param Menu $item
     * @param Request $request
     * @return mixed
     */
    public function updateItem($item, Request $request)
    {
        $languages = $this->languageService->getLanguages();
        $data = $request->request->all();
        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = $item->languageData($language->getKey());
            $languageSpecific->setLanguage($language);
            $languageSpecific->setName($data[$language->getKey()]['name']);
            $languageSpecific->setMenu($item);
            $links = $data[$language->getKey()]['items'];
            foreach ($links as $key => $link) {
                if ($link['page'] == '') {
                    unset($links[$key]);
                }
            }
            $languageSpecific->setLinks($links);
            $this->entityManager->persist($languageSpecific);
        }

        $this->entityManager->flush();
        return $item;
    }
}