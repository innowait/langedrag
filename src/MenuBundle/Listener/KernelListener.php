<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 11:16
 */

namespace MenuBundle\Listener;


use Doctrine\ORM\EntityManager;
use LanguageBundle\Service\LanguageService;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class KernelListener
{
    private $twig;
    private $languageService;
    private $entityManager;

    public function __construct(\Twig_Environment $environment, LanguageService $languageService, EntityManager $entityManager)
    {
        $this->twig = $environment;
        $this->languageService = $languageService;
        $this->entityManager = $entityManager;
    }

    public function onKernelRequest(GetResponseEvent $e)
    {
        $current = $this->languageService->getCurrentLanguage();
        $menus = $this->entityManager->getRepository('MenuBundle:MenuLanguageSpecifics')->findBy(['language' => $current]);
        $menusArray = [];
        foreach ($menus as $menu) {
            $menusArray[$menu->getMenu()->getHash()] = $menu;
        }
        $this->twig->addGlobal('menus', $menusArray);
    }
}