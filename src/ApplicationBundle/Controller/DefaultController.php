<?php

namespace ApplicationBundle\Controller;

use ApplicationBundle\Entity\StaffApplication;
use ApplicationBundle\Entity\StaffDocument;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/submit", name="app_application_submit")
     */
    public function submitApplicationAction(Request $request)
    {
        $application = (new StaffApplication())->fromRequest($request);
        $documentService = $this->get('app.staff.document.service');
        try {
            $this->getDoctrine()->getManager()->persist($application);
            $this->getDoctrine()->getManager()->flush();

            $cscsCopy = $request->files->get('cscs_file');
            if ($cscsCopy && $cscsCopy instanceof UploadedFile) {
                $documentService->submitDocument($cscsCopy, $application, StaffDocument::TYPE_CSCS);
            }

            $passportCopy = $request->files->get('passport_file');
            if ($passportCopy && $passportCopy instanceof UploadedFile) {
                $documentService->submitDocument($passportCopy, $application, StaffDocument::TYPE_PASSPORT);
            }

            $message = new \Swift_Message(
                'SVMK - New Application from Website',
                $this->renderView(':email:application.notification.email.html.twig', [
                    'application' => $application
                ]),
                'text/html',
                'UTF-8'
            );
            $message->setTo([
                'dmitry@kmita.ru',
                'procurement@svmkltd.com',
                'alan@svmkltd.com'
                ]);
            $message->setFrom('hello@innowait.solutions');
            $this->get('mailer')->send($message);
        } catch (\Exception $e) {
            $this->get('logger')->error($e->getMessage());
            $this->addFlash('danger', 'There was some error processing your application, please contact us via email!');
            return $this->redirectToRoute('app_static_single', ['slug' => 'hiring']);
        }
        $this->addFlash('success', 'We have received your application and will contact you as soon as possible!');
        return $this->redirectToRoute('app_static_single', ['slug' => 'hiring']);
    }
}
