<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 03/12/2018
 * Time: 13:08
 */

namespace ApplicationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

/**
 * Class StaffApplication
 * @package Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class StaffApplication
{
    const STATUS_PENDING = 'PENDING';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_REJECTED = 'REJECTED';

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $linkedInLink;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $instagramLink;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(type="string", options={"default": "PENDING"})
     */
    private $status = self::STATUS_PENDING;

    /**
     * @var ArrayCollection|StaffApplicationLanguageSpecific[]
     *
     * @ORM\OneToMany(targetEntity="ApplicationBundle\Entity\StaffApplicationLanguageSpecific", mappedBy="staffApplication", orphanRemoval=true, fetch="EAGER")
     */
    private $languageSpecifics;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var StaffDocument[]
     *
     * @ORM\OneToMany(targetEntity="ApplicationBundle\Entity\StaffDocument", mappedBy="application", orphanRemoval=true)
     */
    private $documents;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $orderNumber;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->documents = new ArrayCollection();
        $this->languageSpecifics = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return StaffDocument[]
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param StaffDocument[] $documents
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;
    }

    /**
     * @return string
     */
    public function getLinkedInLink()
    {
        return $this->linkedInLink;
    }

    /**
     * @param string $linkedInLink
     */
    public function setLinkedInLink($linkedInLink)
    {
        $this->linkedInLink = $linkedInLink;
    }

    /**
     * @return string
     */
    public function getInstagramLink()
    {
        return $this->instagramLink;
    }

    /**
     * @param string $instagramLink
     */
    public function setInstagramLink($instagramLink)
    {
        $this->instagramLink = $instagramLink;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    public function languageData($locale)
    {
        foreach ($this->languageSpecifics as $languageSpecific) {
            if ($languageSpecific->getLanguage()->getKey() == $locale) {
                return $languageSpecific;
            }
        }
        return new StaffApplicationLanguageSpecific();
    }

    /**
     * @return StaffApplicationLanguageSpecific[]|ArrayCollection
     */
    public function getLanguageSpecifics()
    {
        return $this->languageSpecifics;
    }

    /**
     * @param StaffApplicationLanguageSpecific[]|ArrayCollection $languageSpecifics
     */
    public function setLanguageSpecifics($languageSpecifics)
    {
        $this->languageSpecifics = $languageSpecifics;
    }

    /**
     * @return int
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param int $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @param Request $request
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function fromRequest(Request $request)
    {
        $this->email = $request->get('email');
        $this->instagramLink = $request->get('instagram');
        $this->linkedInLink = $request->get('linkedin');
        return $this;
    }
}