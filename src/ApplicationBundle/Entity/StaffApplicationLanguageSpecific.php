<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-11-20
 * Time: 14:03
 */

namespace ApplicationBundle\Entity;


use AppBundle\Entity\Traits\IdEntity;
use AppBundle\Entity\Traits\LanguageSpecific;
use AppBundle\Entity\Traits\Sluggable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class StaffApplicationLanguageSpecific
 * @package ApplicationBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class StaffApplicationLanguageSpecific
{
    use IdEntity;
    use LanguageSpecific;


    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $summary;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $fullname;

    /**
     * @var StaffApplication
     *
     * @ORM\ManyToOne(targetEntity="ApplicationBundle\Entity\StaffApplication", inversedBy="languageSpecifics", cascade={"persist"})
     */
    private $staffApplication;

    /**
     * @return string
     */
    public function getSummary($raw = true)
    {
        if ($raw) {
            return str_replace("\n", '<br>', $this->summary);
        }
        return $this->summary;
    }

    /**
     * @param string $summary
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param string $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @return StaffApplication
     */
    public function getStaffApplication()
    {
        return $this->staffApplication;
    }

    /**
     * @param StaffApplication $staffApplication
     */
    public function setStaffApplication($staffApplication)
    {
        $this->staffApplication = $staffApplication;
    }

    /**
     * @param Request $request
     */
    public function fromRequest(Request $request)
    {
        $this->fullname = $request->get('fullname');
        $this->role = $request->get('role');
        $this->summary = $request->get('summary');
    }
}