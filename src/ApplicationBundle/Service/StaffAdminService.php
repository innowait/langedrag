<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 04/12/2018
 * Time: 11:27
 */

namespace ApplicationBundle\Service;


use AdminBundle\Model\AdminFilter;
use AdminBundle\Service\AbstractAdminService;
use ApplicationBundle\Entity\StaffApplication;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

class StaffAdminService extends AbstractAdminService
{
    protected $listTitle = 'Staff Management';

    /**
     * @var DocumentService
     */
    protected $documentManager;

    public $templates = [
        'list' => "@Application/admin/template/staff/list.template.html.twig",
        'edit' => "@Application/admin/template/staff/edit.template.html.twig"
    ];

    public function getAdditional()
    {
        return [];
    }

    /**
     * @param Request $request
     * @return User|void
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createItem(Request $request)
    {
        /** @var User $item */
        $item = $this->initItem();

        if (!$item->getApplication()) {
            $item->setApplication((new StaffApplication())->fromRequest($request));
        } else {
            $item->getApplication()->fromRequest($request);
        }
        $item->setPlainPassword(md5(time()));

        $this->entityManager->persist($item->getApplication());
        $this->entityManager->flush();

        $item->fromRequest($request);

        $this->entityManager->persist($item);
        $this->entityManager->flush();
        return $item;
    }

    public function getItems(AdminFilter $filter)
    {
        if ($filter->getQuery())
        {
            $query = $this->repository->createQueryBuilder('staff')
                ->select('staff');

            $query->where($query->expr()->like('staff.name', ':query'));
            $query->orWhere($query->expr()->like('staff.lastname', ':query'));
            $query->orWhere($query->expr()->like('staff.email', ':query'));
            $query->setParameter('query', "%{$filter->getQuery()}%");
            return $query->getQuery()->getResult();
        }
        return $this->repository->findAll();
    }

    /**
     * @param User $item
     * @param Request $request
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateItem($item, Request $request)
    {
        if (!$item->getApplication()) {
            $item->setApplication((new StaffApplication())->fromRequest($request));
        } else {
            $item->getApplication()->fromRequest($request);
        }
        $this->entityManager->persist($item->getApplication());
        $this->entityManager->flush();

        $item->fromRequest($request);

        $this->entityManager->persist($item);
        $this->entityManager->flush();
        $file = $request->files->get('file');
        if ($file && $file instanceof UploadedFile) {
            $this->documentManager->submitDocument($file, $item, $request->get('file_type'), $request->get('file_title'));
        }
        return $item;
    }

    /**
     * @return DocumentService
     */
    public function getDocumentManager()
    {
        return $this->documentManager;
    }

    /**
     * @param DocumentService $documentManager
     */
    public function setDocumentManager($documentManager)
    {
        $this->documentManager = $documentManager;
    }
}