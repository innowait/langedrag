<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 04/12/2018
 * Time: 11:27
 */

namespace ApplicationBundle\Service;


use AdminBundle\Service\AbstractAdminService;
use ApplicationBundle\Entity\StaffApplication;
use ApplicationBundle\Entity\StaffApplicationLanguageSpecific;
use LanguageBundle\Entity\Language;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

class ApplicationAdminService extends AbstractAdminService
{
    protected $listTitle = 'Application List';

    public $templates = [
        'list' => "@Application/admin/template/application/list.template.html.twig",
        'edit' => "@Application/admin/template/application/edit.template.html.twig"
    ];

    public function getAdditional()
    {
        return [];
    }

    /**
     * @param Request $request
     * @return StaffApplication
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createItem(Request $request)
    {
        /** @var StaffApplication $item */
        $item = $this->initItem();
        $item->fromRequest($request);

        $data = $request->request->all();
        $languages = $this->languageService->getLanguages();
        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = new StaffApplicationLanguageSpecific();
            $languageSpecific->setLanguage($language);
            $languageSpecific->setFullname($data[$language->getKey()]['fullname']);
            $languageSpecific->setRole($data[$language->getKey()]['role']);
            $languageSpecific->setSummary($data[$language->getKey()]['summary']);
            $languageSpecific->setStaffApplication($item);
            $this->entityManager->persist($languageSpecific);
            $item->getLanguageSpecifics()->add($languageSpecific);
        }

        $lastOrder = (int) $this->entityManager->getRepository($this->repoName)->createQueryBuilder('application')
            ->select('application.orderNumber')
            ->orderBy('application.orderNumber', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult();

        if ($lastOrder) {
            $lastOrder++;
        } else {
            $lastOrder = 1;
        }
        $item->setOrderNumber($lastOrder);

        if ($request->files->get('image') instanceof UploadedFile) {
            $name = $this->fileService->uploadFile($request->files->get('image'), 'staff');
            $item->setImage($name);
        }

        $this->entityManager->persist($item);
        $this->entityManager->flush();
        return $item;
    }

    /**
     * @param StaffApplication $item
     * @param Request $request
     *
     * @return StaffApplication
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function updateItem($item, Request $request)
    {
        $item->fromRequest($request);

        $data = $request->request->all();
        $languages = $this->languageService->getLanguages();
        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = $item->languageData($language->getKey());
            $languageSpecific->setLanguage($language);
            $languageSpecific->setFullname($data[$language->getKey()]['fullname']);
            $languageSpecific->setRole($data[$language->getKey()]['role']);
            $languageSpecific->setSummary($data[$language->getKey()]['summary']);
            $languageSpecific->setStaffApplication($item);
            $this->entityManager->persist($languageSpecific);
        }

        if ($request->files->get('image') instanceof UploadedFile) {
            $name = $this->fileService->uploadFile($request->files->get('image'), 'staff');
            $item->setImage($name);
        }

        $this->entityManager->persist($item);
        $this->entityManager->flush();
        return $item;
    }
}