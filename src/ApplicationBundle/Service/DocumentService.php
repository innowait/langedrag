<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 10/12/2018
 * Time: 09:15
 */

namespace ApplicationBundle\Service;


use ApplicationBundle\Entity\StaffApplication;
use ApplicationBundle\Entity\StaffDocument;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UserBundle\Entity\User;

class DocumentService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Upload and Save Document
     *
     * @param UploadedFile $file
     * @param $userOrApplication
     * @param $fileType
     * @param null $title
     *
     * @return StaffDocument
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function submitDocument(UploadedFile $file, $userOrApplication, $fileType, $title = null)
    {
        $name = time() . '_' . mt_rand(1000,9999) . '.' . strtolower($file->getClientOriginalExtension());
        $file->move(__DIR__ . '/../../../web/uploads/documents', $name);

        $document = new StaffDocument();
        $document->setName($name);
        $document->setType($fileType);
        $document->setTitle($title);
        if ($userOrApplication instanceof User) {
            $document->setUser($userOrApplication);
        }
        if ($userOrApplication instanceof StaffApplication) {
            $document->setApplication($userOrApplication);
        }
        $this->entityManager->persist($document);
        $this->entityManager->flush();
        return $document;
    }
}