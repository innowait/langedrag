<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 16:16
 */

namespace UserBundle\Entity;


use AppBundle\Entity\Traits\IdEntity;
use ApplicationBundle\Entity\StaffApplication;
use ApplicationBundle\Entity\StaffDocument;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class User
 * @package UserBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    const RATE_DAILY = 'DAILY';
    const RATE_HOURLY = 'HOURLY';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $lastname;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phone;

    /**
     * @var StaffDocument[]
     *
     * @ORM\OneToMany(targetEntity="ApplicationBundle\Entity\StaffDocument", mappedBy="user", orphanRemoval=true)
     */
    protected $documents;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $rateType;

    /**
     * @var float
     *
     * @ORM\Column(type="float", options={"default": 0})
     */
    protected $rate = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comments;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getDisplayName()
    {
        $name = $this->name;

        if ($this->lastname) {
            $name .= ' ' . $this->lastname;
        }

        return $name;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return StaffDocument[]
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param StaffDocument[] $documents
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;
    }

    /**
     * @return string
     */
    public function getRateType()
    {
        return $this->rateType;
    }

    /**
     * @param string $rateType
     */
    public function setRateType($rateType)
    {
        $this->rateType = $rateType;
    }

    /**
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @param Request $request
     * @return User
     */
    public function fromRequest(Request $request)
    {
        $this->setEmail($request->get('email'));
        $this->setPhone($request->get('phone'));
        $this->setRateType($request->get('rate_type'));
        $this->setRate($request->get('rate'));
        $this->setComments($request->get('comments'));
        $name = explode(' ', $request->get('fullname'));
        $this->setName($name[0]);
        if (count($name) > 1) {
            $this->setLastname($name[1]);
        }
        $this->setUsername($request->get('email'));
        return $this;
    }
}