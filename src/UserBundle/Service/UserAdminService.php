<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 05/07/2017
 * Time: 00:23
 */

namespace UserBundle\Service;


use AdminBundle\Model\AdminFilter;
use AdminBundle\Service\AbstractAdminService;
use LanguageBundle\Entity\Language;
use LanguageBundle\Entity\Translation;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

class UserAdminService extends AbstractAdminService
{
    public $templates = [
        'list' => "@User/admin/template/users/list.template.html.twig",
        'edit' => "@User/admin/template/users/edit.template.html.twig"
    ];

    /**
     * @param AdminFilter $filter
     * @return array|User[]
     */
    public function getItems(AdminFilter $filter)
    {
        return $this->repository->findAll();
    }

    /**
     * @param $object User
     * @param Request $request
     * @return mixed
     */
    public function updateItem($object, Request $request)
    {
        $data = $request->request->all();

        $object->setUsername($data['email']);
        if ($data['password'] && $data['password'] != '') {
            $object->setPlainPassword($data['password']);
        }
        $object->setName($data['name']);
        $object->setLastname($data['lastname']);
        $object->setEmail($data['email']);
        $object->setRoles([$data['role']]);

        $this->entityManager->persist($object);
        $this->entityManager->flush();
        return $object;
    }

    /**
     * @param $object User
     * @param Request $request
     * @return mixed
     */
    public function createItem(Request $request)
    {
        $data = $request->request->all();
        $object = new User();
        $object->setUsername($data['email']);
        $object->setPlainPassword($data['password']);
        $object->setName($data['name']);
        $object->setLastname($data['lastname']);
        $object->setEmail($data['email']);
        $object->setRoles([$data['role']]);

        $this->entityManager->persist($object);
        $this->entityManager->flush();
        return $object;
    }

    public function deleteItem(AdminFilter $filter)
    {
        $item = $this->getItem($filter);
        $this->entityManager->remove($item);
        $this->entityManager->flush();
    }

    public function getItem(AdminFilter $filter)
    {
        $mainItem = false;
        if ($filter->getId()) {
            $mainItem = $this->repository->findOneBy(['id' => $filter->getId()]);
        }
        return $mainItem;
    }
}