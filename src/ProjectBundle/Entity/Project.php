<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 03/12/2018
 * Time: 14:13
 */

namespace ProjectBundle\Entity;

use AppBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Project
 * @package ProjectBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class Project
{
    const STATUS_INITIATED = 'INITIATED';
    const STATUS_WORKING   = 'WORKING';
    const STATUS_FINISHED  = 'FINISHED';

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $lng;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $startedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $finishedAt;

    /**
     * @var ArrayCollection|ProjectLanguageSpecifics
     *
     * @ORM\OneToMany(targetEntity="ProjectBundle\Entity\ProjectLanguageSpecifics", mappedBy="project", orphanRemoval=true, fetch="EAGER")
     */
    private $languageSpecifics;

    /**
     * @var ProjectMedia[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ProjectBundle\Entity\ProjectMedia", mappedBy="project")
     */
    private $projectMedia;

    public function __construct()
    {
        $this->projectMedia = new ArrayCollection();
        $this->languageSpecifics = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param string $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param string $lng
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * @param \DateTime $startedAt
     */
    public function setStartedAt(\DateTime $startedAt)
    {
        $this->startedAt = $startedAt;
    }

    /**
     * @return \DateTime
     */
    public function getFinishedAt()
    {
        return $this->finishedAt;
    }

    /**
     * @param \DateTime $finishedAt
     */
    public function setFinishedAt($finishedAt)
    {
        $this->finishedAt = $finishedAt;
    }

    /**
     * @return ArrayCollection|ProjectLanguageSpecifics
     */
    public function getLanguageSpecifics()
    {
        return $this->languageSpecifics;
    }

    /**
     * @param ArrayCollection|ProjectLanguageSpecifics $languageSpecifics
     */
    public function setLanguageSpecifics($languageSpecifics)
    {
        $this->languageSpecifics = $languageSpecifics;
    }

    /**
     * @return ProjectMedia[]
     */
    public function getProjectMedia()
    {
        return $this->projectMedia;
    }

    /**
     * @param ProjectMedia[] $projectMedia
     */
    public function setProjectMedia($projectMedia)
    {
        $this->projectMedia = $projectMedia;
    }

    /**
     * @param $locale
     * @return mixed|ProjectLanguageSpecifics
     */
    public function languageData($locale)
    {
        foreach ($this->languageSpecifics as $languageSpecific) {
            if ($languageSpecific->getLanguage()->getKey() == $locale) {
                return $languageSpecific;
            }
        }
        return new ProjectLanguageSpecifics();
    }
}