<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 25/12/2018
 * Time: 20:21
 */

namespace ProjectBundle\Entity;

use AppBundle\Entity\Media;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ProjectMedia
 * @package ProjectBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class ProjectMedia
{
    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="ProjectBundle\Entity\Project", inversedBy="projectMedia")
     */
    private $project;

    /**
     * @var Media
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Media")
     */
    private $media;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param Media $media
     */
    public function setMedia($media)
    {
        $this->media = $media;
    }
}