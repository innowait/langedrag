<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 15:53
 */

namespace ProjectBundle\Entity;


use AppBundle\Entity\Traits\IdEntity;
use AppBundle\Entity\Traits\LanguageSpecific;
use AppBundle\Entity\Traits\Sluggable;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ProjectLanguageSpecifics
 * @package CatalogBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class ProjectLanguageSpecifics
{
    use IdEntity;
    use Sluggable;
    use LanguageSpecific;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="ProjectBundle\Entity\Project", inversedBy="languageSpecifics", cascade={"persist"})
     */
    private $project;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->slug = (new Slugify())->slugify($this->name);
    }
}