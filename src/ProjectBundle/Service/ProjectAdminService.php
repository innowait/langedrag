<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 22:01
 */

namespace ProjectBundle\Service;


use AdminBundle\Service\AbstractAdminService;
use CatalogBundle\Entity\Item;
use CatalogBundle\Entity\ItemLanguageSpecifics;
use LanguageBundle\Entity\Language;
use ProjectBundle\Entity\Project;
use ProjectBundle\Entity\ProjectLanguageSpecifics;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class ProjectAdminService extends AbstractAdminService
{
    protected $listTitle = 'Projects List';

    public $templates = [
        'list' => "@Project/admin/template/testimonial/list.template.html.twig",
        'edit' => "@Project/admin/template/testimonial/edit.template.html.twig"
    ];

    public function createItem(Request $request)
    {
        $languages = $this->languageService->getLanguages();
        /** @var Project $item */
        $item = $this->initItem();
        $data = $request->request->all();

        $item->setStatus($data['status']);
        $item->setLat($data['lat']);
        $item->setLng($data['lng']);
        $item->setStartedAt(new \DateTime($data['start']));
        if ($data['finish']) {
            $item->setFinishedAt(new \DateTime($data['finish']));
        } else {
            $item->setFinishedAt(null);
        }

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = new ProjectLanguageSpecifics();
            $languageSpecific->setLanguage($language);
            $languageSpecific->setName($data[$language->getKey()]['name']);
            $languageSpecific->setDescription($data[$language->getKey()]['description']);
            $languageSpecific->setProject($item);
            $this->entityManager->persist($languageSpecific);
            $item->getLanguageSpecifics()->add($languageSpecific);
        }

        $this->entityManager->flush();
        return $item;
    }

    /**
     * @param Project $item
     * @param Request $request
     * @return mixed
     */
    public function updateItem($item, Request $request)
    {
        $languages = $this->languageService->getLanguages();
        $data = $request->request->all();

        $item->setStatus($data['status']);
        $item->setLat($data['lat']);
        $item->setLng($data['lng']);
        $item->setStartedAt(new \DateTime($data['start']));
        if ($data['finish']) {
            $item->setFinishedAt(new \DateTime($data['finish']));
        } else {
            $item->setFinishedAt(null);
        }

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = $item->languageData($language->getKey());
            $languageSpecific->setLanguage($language);
            $languageSpecific->setName($data[$language->getKey()]['name']);
            $languageSpecific->setDescription($data[$language->getKey()]['description']);
            $languageSpecific->setProject($item);
            $this->entityManager->persist($languageSpecific);
        }

        $this->entityManager->flush();
        return $item;
    }
}