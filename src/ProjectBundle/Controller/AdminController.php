<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 26/12/2018
 * Time: 12:38
 */

namespace ProjectBundle\Controller;


use AppBundle\Entity\Media;
use ProjectBundle\Entity\ProjectMedia;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends Controller
{
    /**
     * @Route("/admin/testimonial/{id}/media/upload", name="app_project_media_upload")
     */
    public function uploadMedia($id, Request $request)
    {
        $fileService = $this->get('app.service.file');
        $file = $request->files->get('file');

        $name = $fileService->uploadFile($file, 'testimonial');
        $project = $this->getDoctrine()->getRepository('ProjectBundle:Project')->findOneBy(['id' => $id]);

        $mediaItem = new Media();
        $mediaItem->setFilename($name);
        $this->getDoctrine()->getManager()->persist($mediaItem);

        $projectMedia = new ProjectMedia();
        $projectMedia->setMedia($mediaItem);
        $projectMedia->setProject($project);
        $this->getDoctrine()->getManager()->persist($projectMedia);

        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'success' => true
        ]);
    }

    /**
     * @Route("/admin/testimonial/{testimonial}/media/{media}/delete", name="app_project_media_delete")
     */
    public function deleteMedia($project, $media, Request $request)
    {
        $project = $this->getDoctrine()->getRepository('ProjectBundle:Project')->findOneBy(['id' => $project]);
        $media = $this->getDoctrine()->getRepository('ProjectBundle:ProjectMedia')->findOneBy(['id' => $media]);

        $this->getDoctrine()->getManager()->remove($media);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'success' => true
        ]);
    }
}