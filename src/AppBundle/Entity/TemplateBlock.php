<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 12/12/2017
 * Time: 15:15
 */

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\IdEntity;
use AppBundle\Entity\Traits\LanguageSpecific;
use CatalogBundle\Entity\Category;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class TemplateBlock
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class TemplateBlock
{
    use IdEntity;

    /**
     * @var ArrayCollection|TemplateBlockLanguageSpecifics
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TemplateBlockLanguageSpecifics", mappedBy="block", fetch="EAGER", cascade={"persist"}, orphanRemoval=true)
     */
    private $languageSpecifics;

    /**
     * @var Category[]
     *
     * @ORM\ManyToMany(targetEntity="CatalogBundle\Entity\Category", mappedBy="blocks")
     */
    private $categories;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $imageBW;

    public function __construct()
    {
        $this->languageSpecifics = new ArrayCollection();
    }

    /**
     * @return TemplateBlockLanguageSpecifics|ArrayCollection
     */
    public function getLanguageSpecifics()
    {
        return $this->languageSpecifics;
    }

    /**
     * @param TemplateBlockLanguageSpecifics|ArrayCollection $languageSpecifics
     */
    public function setLanguageSpecifics($languageSpecifics)
    {
        $this->languageSpecifics = $languageSpecifics;
    }

    /**
     * @return \CatalogBundle\Entity\Category[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param \CatalogBundle\Entity\Category[] $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getImageBW()
    {
        return $this->imageBW;
    }

    /**
     * @param string $imageBW
     */
    public function setImageBW($imageBW)
    {
        $this->imageBW = $imageBW;
    }

    public function languageData($locale)
    {
        foreach ($this->languageSpecifics as $languageSpecific) {
            if ($languageSpecific->getLanguage()->getKey() == $locale) {
                return $languageSpecific;
            }
        }
        return new TemplateBlockLanguageSpecifics();
    }
}