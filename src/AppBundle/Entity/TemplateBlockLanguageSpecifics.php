<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 15:53
 */

namespace AppBundle\Entity;


use AppBundle\Entity\Traits\IdEntity;
use AppBundle\Entity\Traits\LanguageSpecific;
use AppBundle\Entity\Traits\Sluggable;
use AppBundle\Entity\Traits\SluggableLanguageSpecific;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class TemplateBlockLanguageSpecifics
 * @package CatalogBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class TemplateBlockLanguageSpecifics
{
    use IdEntity;
    use LanguageSpecific;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @var TemplateBlock
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TemplateBlock", inversedBy="languageSpecifics", cascade={"persist"})
     */
    private $block;

    /**
     * @return string
     */
    public function getContent()
    {
        return str_replace("&nbsp;", " ", $this->content);
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return TemplateBlock
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * @param TemplateBlock $block
     */
    public function setBlock($block)
    {
        $this->block = $block;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}