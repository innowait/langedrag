<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 15:54
 */

namespace AppBundle\Entity\Traits;


use Doctrine\ORM\Mapping as ORM;
use LanguageBundle\Entity\Language;

trait LanguageSpecific
{
    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="LanguageBundle\Entity\Language")
     */
    protected $language;

    /**
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param Language $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }
}