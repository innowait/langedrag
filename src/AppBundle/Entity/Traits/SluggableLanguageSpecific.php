<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 15:47
 */

namespace AppBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait SluggableLanguageSpecific
{
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string")
     */
    protected $slug;

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
}