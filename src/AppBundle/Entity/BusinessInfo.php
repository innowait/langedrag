<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-09-30
 * Time: 12:01
 */

namespace AppBundle\Entity;


use AppBundle\Entity\Traits\IdEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BusinessInfo
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 */
class BusinessInfo
{
    use IdEntity;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     */
    private $workingHours = [];

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $logo;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     */
    private $socialLinks = [];

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $workingEmail;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $workingPhone;

    /**
     * @return array
     */
    public function getWorkingHours()
    {
        return $this->workingHours;
    }

    /**
     * @param array $workingHours
     */
    public function setWorkingHours($workingHours)
    {
        $this->workingHours = $workingHours;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return array
     */
    public function getSocialLinks()
    {
        return $this->socialLinks;
    }

    /**
     * @param array $socialLinks
     */
    public function setSocialLinks($socialLinks)
    {
        $this->socialLinks = $socialLinks;
    }

    /**
     * @return string
     */
    public function getWorkingEmail()
    {
        return $this->workingEmail;
    }

    /**
     * @param string $workingEmail
     */
    public function setWorkingEmail($workingEmail)
    {
        $this->workingEmail = $workingEmail;
    }

    /**
     * @return string
     */
    public function getWorkingPhone()
    {
        return $this->workingPhone;
    }

    /**
     * @param string $workingPhone
     */
    public function setWorkingPhone($workingPhone)
    {
        $this->workingPhone = $workingPhone;
    }

    public function isWorkingNow()
    {
        $timezone = new \DateTimeZone('Europe/London');
        $now = new \DateTime('now', $timezone);
        $weekDay = date('l', $now->getTimestamp());
        if (!isset($this->getWorkingHours()[strtolower($weekDay)])) {
            return false;
        }
        $openHours = $this->getWorkingHours()[strtolower($weekDay)];
        if (strtotime($now->format('H:i:s')) >= strtotime($openHours['start']) && strtotime($now->format('H:i:s')) < strtotime($openHours['finish'])) {
            return true;
        }
        return false;
    }
}