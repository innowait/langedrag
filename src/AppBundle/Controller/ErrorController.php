<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 14/02/2019
 * Time: 15:33
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ErrorController extends Controller
{
    /**
     * @Route("/error/{code}", name="app_error_page")
     *
     * @param $code
     * @return Response
     */
    public function errorPage($code)
    {
        if (!in_array($code, [500, 404, 200])) {
            $code = 500;
        }
        $response = new Response();
        $response->setContent($this->renderView('@App/errors/general.error.template.html.twig', ['code' => $code]));
        $response->setStatusCode($code);
        return $response;
    }
}