<?php

namespace AppBundle\Controller;

use CatalogBundle\Entity\Category;
use ApplicationBundle\Entity\StaffApplication;
use CatalogBundle\Entity\ItemLanguageSpecifics;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use StaticPageBundle\Entity\StaticPage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->redirectToRoute('homepage_language', ['_locale' => $request->getLocale()]);
    }

    /**
     * @Route("/v2", name="homepage_v2")
     */
    public function v2Action(Request $request)
    {
        /** @var EntityRepository $slideRepo */
        $slideRepo = $this->getDoctrine()->getRepository('CatalogBundle:Slide');
        $slides = $slideRepo->createQueryBuilder('slide')
            ->select('slide, languageSpecifics')
            ->leftJoin('slide.languageSpecifics', 'languageSpecifics')
            ->leftJoin('languageSpecifics.language', 'language')
            ->where('languageSpecifics.language = :language')
            ->orderBy('slide.order', 'ASC')
            ->setParameter('language', $this->get('app.language.service')->getCurrentLanguage())
            ->getQuery()
            ->getResult();

        // replace this example code with whatever you need
        /** @var EntityRepository $testimonialsRepo */
        $testimonialsRepo = $this->getDoctrine()->getRepository('InnowaitFeedbackBundle:Testimonial');
        $testimonials = $testimonialsRepo->createQueryBuilder('testimonials')
            ->select('testimonials, languageSpecifics')
            ->leftJoin('testimonials.languageSpecifics', 'languageSpecifics')
            ->where('languageSpecifics.language = :language')
            ->setParameter('language', $this->get('app.language.service')->getCurrentLanguage())
            ->getQuery()
            ->getResult();

        return $this->render(':page:v2.page.html.twig', [
            'slides' => $slides,
            'testimonials' => $testimonials
        ]);
    }

    /**
     * @Route("/{_locale}", name="homepage_language")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function languageRedirectAction(Request $request)
    {
        // replace this example code with whatever you need
        /** @var EntityRepository $testimonialsRepo */
        $testimonialsRepo = $this->getDoctrine()->getRepository('InnowaitFeedbackBundle:Testimonial');
        $testimonials = $testimonialsRepo->createQueryBuilder('testimonials')
            ->select('testimonials, languageSpecifics')
            ->leftJoin('testimonials.languageSpecifics', 'languageSpecifics')
            ->where('languageSpecifics.language = :language')
            ->setParameter('language', $this->get('app.language.service')->getCurrentLanguage())
            ->getQuery()
            ->getResult();

        /** @var ItemLanguageSpecifics[] $services */
        $services = $this->getDoctrine()->getRepository('CatalogBundle:ItemLanguageSpecifics')
            ->findBy(['language' => $this->get('app.language.service')->getCurrentLanguage()]);

        /** @var EntityRepository $slideRepo */
        $slideRepo = $this->getDoctrine()->getRepository('CatalogBundle:Slide');
        $slides = $slideRepo->createQueryBuilder('slide')
            ->select('slide, languageSpecifics')
            ->leftJoin('slide.languageSpecifics', 'languageSpecifics')
            ->leftJoin('languageSpecifics.language', 'language')
            ->where('languageSpecifics.language = :language')
            ->orderBy('slide.order', 'ASC')
            ->setParameter('language', $this->get('app.language.service')->getCurrentLanguage())
            ->getQuery()
            ->getResult();

        $gallery = $this->getDoctrine()->getRepository('CatalogBundle:GalleryImage')->findAll();

        $ref = $request->headers->get('referer');
        $intro = true;
        if ($ref && strpos($ref, 'langehouse.se') > -1) {
            $intro = false;
        }
        return $this->render(':page:index.page.html.twig', [
            'testimonials' => $testimonials,
            'services' => $services,
            'slides' => $slides,
            'gallery' => $gallery,
            'intro' => $intro
        ]);
    }

    /**
     * @param Request $request
     * @Route("/{_locale}/cookies", name="app_cookies_text", options={"expose": true})
     */
    public function cookiesTextAction(Request $request)
    {
        return new JsonResponse([
            'success' => true,
            'content' => $this->get('app.language.service')->getTranslation('cookies-text', 'misc'),
            'title' => $this->get('app.language.service')->getTranslation('cookies-title', 'misc'),
        ]);
    }
}
