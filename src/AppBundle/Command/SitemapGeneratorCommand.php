<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 13/12/2018
 * Time: 17:52
 */

namespace AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SitemapGeneratorCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName('app:sitemap:generate')
            ->addOption('host', 'wh', InputOption::VALUE_REQUIRED, 'Please enter application host.')
            ->addOption('schema', 'ws', InputOption::VALUE_OPTIONAL, 'Website Scheme (http/https)', 'https');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $sitemapGenerator = $this->getContainer()->get('app.sitemap.generator');
        $sitemapGenerator->configure(
            $input->getOption('schema'),
            $input->getOption('host')
        );
        $xml = $sitemapGenerator->generateSitemap();
        file_put_contents(__DIR__ . '/../../../web/sitemap.xml', $xml);
    }
}