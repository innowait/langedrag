<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-08-06
 * Time: 13:58
 */

namespace AppBundle\Command;


use CatalogBundle\Entity\Item;
use CatalogBundle\Entity\Slide;
use Innowait\FeedbackBundle\Entity\Testimonial;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixImagesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:fix:images');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->fixCatalogItems($input, $output);
        $this->fixTestimonials($input, $output);
        $this->fixCatalogCategories($input, $output);
    }

    private function fixCatalogItems(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $products = $em->getRepository('CatalogBundle:Slide')->findAll();
        /** @var Slide $slide */
        foreach ($products as $slide) {
            $dir = __DIR__ . '/../../../web/uploads/slide/';
            $mobileDir = __DIR__ . '/../../../web/uploads/mobile_slide/';
            $newName = str_replace('.jpg', '.webp', $slide->getImage());
            $newMobileName = str_replace('.jpg', '.webp', $slide->getMobileImage());
            try {
                $img = imagecreatefromjpeg($dir . 'bg_' . $slide->getImage());
                $mobileImg = imagecreatefromjpeg($mobileDir . 'bg_' . $slide->getMobileImage());
                imagewebp($img, $dir . $newName, 100);
                imagewebp($mobileImg, $mobileDir . $newMobileName, 100);
                $output->writeln('[>] Slide Transferred ' . $newName);
            } catch (\Exception $exception) {
                $output->writeln('[>] Slide Skipping ' . $newName);
            }
        }
        $output->writeln('[>] Finished image transformations');
    }

    private function fixCatalogCategories(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $products = $em->getRepository('CatalogBundle:Item')->findAll();
        /** @var Item $slide */
        foreach ($products as $slide) {
            $dir = __DIR__ . '/../../../web/uploads/catalog-item/';
            $newName = str_replace('.jpg', '.webp', $slide->getImage());
            try {
                $img = imagecreatefromjpeg($dir . 'thumb_' . $slide->getImage());
                imagewebp($img, $dir . $newName, 100);
                $output->writeln('[>] Catalog Item Transferred ' . $newName);
            } catch (\Exception $exception) {
                $output->writeln('[>] Catalog Item Skipping ' . $newName);
            }
        }
        $output->writeln('[>] Finished image transformations');
    }

    private function fixTestimonials(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $products = $em->getRepository('InnowaitFeedbackBundle:Testimonial')->findAll();
        /** @var Testimonial $testimonial */
        foreach ($products as $testimonial) {
            $dir = __DIR__ . '/../../../web/uploads/testimonial/';
            $newName = str_replace('.png', '.webp', $testimonial->getImage());
            try {
                if (strpos($testimonial->getImage(), 'jpg') > -1) {
                    $img = imagecreatefromjpeg($dir . 'thumb_' . $testimonial->getImage());
                } else {
                    $img = imagecreatefrompng($dir . 'thumb_' . $testimonial->getImage());
                }
                imagewebp($img, $dir . $newName, 100);

                $output->writeln('[>] Testimonial Transferred ' . $newName);
            } catch (\Exception $exception) {
                $output->writeln('[>] Testimonial Skipping ' . $newName);
            }
        }
        $output->writeln('[>] Finished image transformations');
    }

}