<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 13/12/2018
 * Time: 17:28
 */

namespace AppBundle\Service;


class SitemapGenerator
{
    /**
     * @var SitemapServiceInterface[]
     */
    private $services = [];

    /**
     * @param SitemapServiceInterface $service
     */
    public function addService(SitemapServiceInterface $service)
    {
        $this->services[] = $service;
    }

    /**
     * Generate Sitemap
     *
     * @return string
     */
    public function generateSitemap()
    {
        $links = [];
        foreach ($this->services as $service) {
            $resultLinks = $service->getLinks();
            if (!empty($resultLinks)) {
                $links = array_merge($links, $resultLinks);
            }
        }

        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">';

        /** @var SitemapLink $data */
        foreach ($links as $data) {
            $xml .= $data->getXml();
        }
        $xml .= '</urlset>';

        return $xml;
    }

    /**
     * Configure each service with correct base url
     *
     * @param $scheme
     * @param $host
     */
    public function configure($scheme, $host)
    {
        foreach ($this->services as $service) {
            $service->configure($scheme, $host);
        }
    }
}