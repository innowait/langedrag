<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 13/12/2018
 * Time: 17:29
 */

namespace AppBundle\Service;


interface SitemapServiceInterface
{
    public function getLinks();

    public function configure($scheme, $host);
}