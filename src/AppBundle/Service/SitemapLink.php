<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 13/12/2018
 * Time: 17:42
 */

namespace AppBundle\Service;


class SitemapLink
{
    const FREQUENCY_ALWAYS = 'always';
    const FREQUENCY_HOURLY = 'hourly';
    const FREQUENCY_DAILY = 'daily';
    const FREQUENCY_WEEKLY = 'weekly';
    const FREQUENCY_MONTHLY = 'monthly';
    const FREQUENCY_YEARLY = 'yearly';
    const FREQUENCY_NEVER = 'never';

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $alternates = [];

    /**
     * YYYY-MM-DD
     * @var \DateTime
     */
    private $lastModified;

    /**
     * @var string
     */
    private $changeFreq = self::FREQUENCY_WEEKLY;

    /**
     * @var string
     */
    private $priority = '0.5';

    public function __construct()
    {
        $this->lastModified = new \DateTime();
    }

    /**
     * @return string
     */
    public function getChangeFreq()
    {
        return $this->changeFreq;
    }

    /**
     * @param string $changeFreq
     */
    public function setChangeFreq($changeFreq)
    {
        $this->changeFreq = $changeFreq;
    }

    /**
     * @return \DateTime
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * @param \DateTime $lastModified
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;
    }

    /**
     * @return string
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param string $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return array
     */
    public function getAlternates()
    {
        return $this->alternates;
    }

    /**
     * @param array $alternates
     */
    public function setAlternates($alternates)
    {
        $this->alternates = $alternates;
    }

    /**
     * @return string
     */
    public function getXml()
    {
        $alternateHtml = '';
        foreach ($this->getAlternates() as $alternate) {
            $alternateHtml .= '
                <xhtml:link
                 rel="alternate"
                 hreflang="'.$alternate['lang'].'"
                 href="'.$alternate['url'].'"
                 />
             ';
        }

        return '
              <url>
                <loc>'.$this->getUrl().'</loc>
                '.$alternateHtml.'
                <lastmod>'.$this->getLastModified()->format('Y-m-d').'</lastmod>
                <changefreq>'.$this->getChangeFreq().'</changefreq>
                <priority>'.$this->getPriority().'</priority>
              </url>
        ';
    }
}