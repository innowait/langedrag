<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 22:01
 */

namespace AppBundle\Service;


use AdminBundle\Service\AbstractAdminService;
use AppBundle\Entity\TemplateBlock;
use AppBundle\Entity\TemplateBlockLanguageSpecifics;
use CatalogBundle\Entity\Item;
use CatalogBundle\Entity\ItemLanguageSpecifics;
use LanguageBundle\Entity\Language;
use StaticPageBundle\Entity\StaticPage;
use StaticPageBundle\Entity\StaticPageLanguageSpecific;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class TemplateBlockAdminService extends AbstractAdminService
{
    protected $listTitle = 'Blocks List';

    public $templates = [
        'list' => "admin/template/blocks/list.template.html.twig",
        'edit' => "admin/template/blocks/edit.template.html.twig"
    ];

    public function createItem(Request $request)
    {
        $languages = $this->languageService->getLanguages();
        /** @var TemplateBlock $item */
        $item = $this->initItem();
        $data = $request->request->all();

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = new TemplateBlockLanguageSpecifics();
            $languageSpecific->setLanguage($language);
            $languageSpecific->setTitle($data[$language->getKey()]['title']);
            $languageSpecific->setContent($data[$language->getKey()]['content']);
            $languageSpecific->setBlock($item);
            $this->entityManager->persist($languageSpecific);
            $item->getLanguageSpecifics()->add($languageSpecific);
        }

        $image = $request->files->get('image');
        if ($image instanceof UploadedFile) {
            $item->setImage($this->fileService->uploadFile($image, 'template-block'));
        }

        $image = $request->files->get('image-bw');
        if ($image instanceof UploadedFile) {
            $item->setImageBW($this->fileService->uploadFile($image, 'template-block'));
        }

        $this->entityManager->flush();
        return $item;
    }

    /**
     * @param TemplateBlock $item
     * @param Request $request
     * @return mixed
     */
    public function updateItem($item, Request $request)
    {
        $languages = $this->languageService->getLanguages();
        $data = $request->request->all();

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = $item->languageData($language->getKey());
            $languageSpecific->setLanguage($language);
            $languageSpecific->setTitle($data[$language->getKey()]['title']);
            $languageSpecific->setContent($data[$language->getKey()]['content']);
            $languageSpecific->setBlock($item);
            $this->entityManager->persist($languageSpecific);
        }

        $image = $request->files->get('image');
        if ($image instanceof UploadedFile) {
            $item->setImage($this->fileService->uploadFile($image, 'template-block'));
        }

        $image = $request->files->get('image-bw');
        if ($image instanceof UploadedFile) {
            $item->setImageBW($this->fileService->uploadFile($image, 'template-block'));
        }

        $this->entityManager->flush();
        return $item;
    }
}