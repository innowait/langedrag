<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 13/12/2018
 * Time: 17:49
 */

namespace AppBundle\Service;


use AppBundle\Service\AbstractSitemapService;
use AppBundle\Service\SitemapLink;
use Symfony\Component\Routing\Router;

class GeneralSitemapService extends AbstractSitemapService
{
    public function getLinks()
    {
        $languages = $this->entityManager->getRepository('LanguageBundle:Language')->findBy(['isEnabled' => true]);
        $links = [];

        foreach ($languages as $language) {
            $link = new SitemapLink();
            $link->setUrl($this->router->generate(
                'homepage_language',
                ['_locale' => $language->getKey()],
                Router::ABSOLUTE_URL
            ));
            $links[] = $link;
        }

        return $links;
    }
}