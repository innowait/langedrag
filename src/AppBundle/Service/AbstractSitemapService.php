<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 13/12/2018
 * Time: 17:48
 */

namespace AppBundle\Service;


use Doctrine\ORM\EntityManager;
use LanguageBundle\Service\LanguageService;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

abstract class AbstractSitemapService implements SitemapServiceInterface
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * AbstractSitemapService constructor.
     *
     * @param RouterInterface $router
     * @param EntityManager $entityManager
     */
    public function __construct(RouterInterface $router, EntityManager $entityManager)
    {
        $this->router = $router;
        $this->router->getContext()->setScheme('https');
        $this->router->getContext()->setHost('demo.innowait.solutions');
        $this->entityManager = $entityManager;
    }

    /**
     * Configure Sitemap Generator
     *
     * @param $scheme
     * @param $host
     */
    public function configure($scheme, $host)
    {
        $this->router->getContext()->setScheme($scheme);
        $this->router->getContext()->setHost($host);
    }
}