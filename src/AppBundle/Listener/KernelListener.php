<?php
/**
 * Created by PhpStorm.
 * User: dmitry.kmita
 * Date: 2019-05-07
 * Time: 14:39
 */

namespace AppBundle\Listener;


use AppBundle\Entity\BusinessInfo;
use CatalogBundle\Entity\Category;
use Doctrine\Common\Cache\ApcCache;
use Doctrine\Common\Cache\CacheProvider;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\Common\Cache\MemcacheCache;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class KernelListener
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var FilesystemCache
     */
    private $cache;

    /**
     * @var string
     */
    private $env;

    public function __construct(EntityManagerInterface $entityManager, \Twig_Environment $twig, $cache, $env)
    {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        $this->cache = $cache;
        $this->env = $env;
    }

    public function onRequest(GetResponseEvent $e)
    {
        $request = $e->getRequest();

        if ($request->getMethod() == Request::METHOD_GET && $this->env !== 'dev' && strpos($request->getUri(), '/admin/') == -1) {
            $cache = $this->cache->fetch($request->getUri());
            if ($cache) {
                if ($cache['status'] == 200) {
                    $response = new Response();
                    $response->setStatusCode($cache['status']);
                    $response->setContent($cache['body']);
                    $e->setResponse($response);
                    return;
                }
            }
        }
        $locale = $request->getLocale();
        $language = $this->entityManager->getRepository('LanguageBundle:Language')->findOneBy(['key' => $locale]);
        /** @var EntityRepository $repo */
        $repo = $this->entityManager->getRepository('CatalogBundle:Category');
        $query = $repo->createQueryBuilder('category')
            ->select('category, languageSpecifics')
            ->leftJoin('category.languageSpecifics', 'languageSpecifics')
            ->leftJoin('languageSpecifics.language', 'language')
            ->where('language.key = :language')
            ->setParameter('language', $language->getKey())
            ->orderBy('category.order', 'ASC');

        $catalog = $query->getQuery()->getResult();
        $result = [];
        /** @var Category $item */
        foreach ($catalog as $item) {
            $result[] = $item->languageData($language->getKey());
        }
        $this->twig->addGlobal('catalogCategories', $result);

        $business = $this->entityManager->getRepository('AppBundle:BusinessInfo')->findAll();
        if (isset($business[0])) {
            $business = $business[0];
        } else {
            $business = new BusinessInfo();
        }
        $this->twig->addGlobal('business', $business);
    }

    public function onResponse(FilterResponseEvent $e) {
        $response = $e->getResponse();
        $request = $e->getRequest();
        $body = $response->getContent();
        $status = $response->getStatusCode();
        $this->cache->save($request->getUri(), [
            'status' => $status,
            'body' => $body
        ], 60 * 5);
    }
}