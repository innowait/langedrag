<?php
/**
 * Created by PhpStorm.
 * User: kmitad
 * Date: 13/12/2018
 * Time: 17:49
 */

namespace StaticPageBundle\Service;


use AppBundle\Service\AbstractSitemapService;
use AppBundle\Service\SitemapLink;
use Symfony\Component\Routing\Router;

class StaticPageSitemapService extends AbstractSitemapService
{
    public function getLinks()
    {
        $pages = $this->entityManager->getRepository('StaticPageBundle:StaticPageLanguageSpecific')->findAll();

        $links = [];
        foreach ($pages as $page) {
            $link = new SitemapLink();
            $link->setUrl($this->router->generate(
                'app_static_single',
                [
                    'slug' => $page->getSlug(),
                    '_locale' => $page->getLanguage()->getKey()
                ],
                Router::ABSOLUTE_URL
            ));
            $links[] = $link;
        }
        return $links;
    }
}