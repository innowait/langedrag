<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 22:01
 */

namespace StaticPageBundle\Service;


use AdminBundle\Model\AdminFilter;
use AdminBundle\Service\AbstractAdminService;
use CatalogBundle\Entity\Item;
use CatalogBundle\Entity\ItemLanguageSpecifics;
use LanguageBundle\Entity\Language;
use StaticPageBundle\Entity\StaticPage;
use StaticPageBundle\Entity\StaticPageLanguageSpecific;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class StaticPageAdminService extends AbstractAdminService
{
    protected $listTitle = 'Pages List';

    public $templates = [
        'list' => "@StaticPage/admin/template/static_pages/list.template.html.twig",
        'edit' => "@StaticPage/admin/template/static_pages/edit.template.html.twig"
    ];

    public function getAdditional()
    {
        $categories = $this->entityManager->getRepository('CatalogBundle:Category')->findAll();
        $parent = $this->entityManager->getRepository('StaticPageBundle:StaticPageLanguageSpecific')->findAll();
        return [
            'categories' => $categories,
            'parent' => $parent
        ];
    }

    public function getItems(AdminFilter $filter)
    {
        return $this->repository->findBy(['parent' => null]);
    }

    public function createItem(Request $request)
    {
        $languages = $this->languageService->getLanguages();
        /** @var StaticPage $item */
        $item = $this->initItem();
        $data = $request->request->all();

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = new StaticPageLanguageSpecific();
            $languageSpecific->setLanguage($language);
            $languageSpecific->setTitle($data[$language->getKey()]['title']);
            $languageSpecific->setCaption($data[$language->getKey()]['caption']);
            $languageSpecific->setContent($data[$language->getKey()]['content']);
            $languageSpecific->setStaticPage($item);
            $this->entityManager->persist($languageSpecific);
            $item->getLanguageSpecifics()->add($languageSpecific);
        }

        $parent = $request->get('parent');
        if ($parent) {
            $parent = $this->repository->findOneBy(['id' => $parent]);
            $item->setParent($parent);
        }
        $item->setVideos($request->get('videos', []));
        $item->setType($data['type']);

        $image = $request->files->get('background');
        if ($image instanceof UploadedFile) {
            $item->setBackground($this->fileService->uploadFile($image, 'static-page'));
        }

        $this->entityManager->flush();
        return $item;
    }

    /**
     * @param StaticPage $item
     * @param Request $request
     * @return mixed
     */
    public function updateItem($item, Request $request)
    {
        $languages = $this->languageService->getLanguages();
        $data = $request->request->all();

        /** @var Language $language */
        foreach ($languages as $language) {
            $languageSpecific = $item->languageData($language->getKey());
            $languageSpecific->setLanguage($language);
            $languageSpecific->setTitle($data[$language->getKey()]['title']);
            $languageSpecific->setCaption($data[$language->getKey()]['caption']);
            $languageSpecific->setContent($data[$language->getKey()]['content']);
            $languageSpecific->setStaticPage($item);

            $this->entityManager->persist($languageSpecific);
        }

        $item->setType($data['type']);

        $parent = $request->get('parent');
        if ($parent) {
            $parent = $this->repository->findOneBy(['id' => $parent]);
            $item->setParent($parent);
        }

        $item->setVideos($request->get('videos', []));
        $image = $request->files->get('background');
        if ($image instanceof UploadedFile) {
            $item->setBackground($this->fileService->uploadFile($image, 'static-page'));
        }
        $this->entityManager->flush();
        return $item;
    }
}