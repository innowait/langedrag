<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 11/12/2017
 * Time: 21:11
 */

namespace StaticPageBundle\Repository;


use Doctrine\ORM\EntityRepository;

class StaticPageRepository extends EntityRepository
{
    public function getAllPages()
    {
        return $this->createQueryBuilder('pages')
            ->select('pages')
            ->leftJoin('pages.languageSpecifics', 'language_specifics')
            ->addSelect('language_specifics')
            ->getQuery()
            ->getResult();
    }
}