<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 11/12/2017
 * Time: 23:07
 */

namespace StaticPageBundle\Listener;

use Doctrine\ORM\EntityManager;
use StaticPageBundle\Entity\StaticPage;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class KernelListener
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(\Twig_Environment $twig, EntityManager $entityManager)
    {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
    }

    /**
     * On Kernel Request
     *
     * @param $e
     */
    public function onKernelRequest(GetResponseEvent $e)
    {
        $pages = $this->entityManager->getRepository('StaticPageBundle:StaticPage')->getAllPages();
        $pageArray = [];

        /** @var StaticPage $page */
        foreach ($pages as $page) {
            $pageArray[$page->getId()] = $page->languageData($e->getRequest()->getLocale())->getSlug();
        }
        $this->twig->addGlobal('availablePages', $pageArray);
    }
}