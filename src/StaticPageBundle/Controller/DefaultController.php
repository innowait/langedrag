<?php

namespace StaticPageBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Innowait\FeedbackBundle\Entity\FaqLanguageSpecific;
use StaticPageBundle\Entity\StaticPage;
use StaticPageBundle\Entity\StaticPageLanguageSpecific;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/{slug}", name="app_static_single")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($slug, Request $request)
    {
        $current = $this->get('app.language.service')->getCurrentLanguage();

        /** @var StaticPageLanguageSpecific $page */
        /** @var EntityRepository $pageRepo */
        $pageRepo = $this->getDoctrine()->getRepository('StaticPageBundle:StaticPageLanguageSpecific');

        /** @todo: move to separate service */
        $page = $pageRepo->createQueryBuilder('page')
            ->select('page, originalPage, language')
            ->leftJoin('page.staticPage', 'originalPage')
            ->leftJoin('page.language', 'language')
            ->where('page.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if ($page->getLanguage() != $current) {
            $new = $this->getDoctrine()
                ->getRepository('StaticPageBundle:StaticPageLanguageSpecific')
                ->findOneBy(['staticPage' => $page->getStaticPage(), 'language' => $current]);
            return $this->redirectToRoute('app_static_single', ['slug' => $new->getSlug(), 'service' => $request->get('service')]);
        }

        if ($page->getStaticPage()->getType() == StaticPage::TYPE_ABOUT) {
            return $this->render('page/about.page.html.twig', [
                'page' => $page->getStaticPage(),
                'blocks' => $this->getDoctrine()->getRepository('AppBundle:TemplateBlockLanguageSpecifics')
                                ->findBy(['language' => $current])
            ]);
        }


        if ($page->getStaticPage()->getType() == StaticPage::TYPE_BASIC) {
            return $this->render('page/basic.page.html.twig', [
                'page' => $page->getStaticPage(),
                'localPage' => $page,
                'blocks' => $this->getDoctrine()->getRepository('AppBundle:TemplateBlockLanguageSpecifics')
                    ->findBy(['language' => $current])
            ]);
        }

        if ($page->getStaticPage()->getType() == StaticPage::TYPE_GENERAL) {
            return $this->render('page/general.page.html.twig', [
                'page' => $page->getStaticPage(),
                'localPage' => $page,
                'blocks' => $this->getDoctrine()->getRepository('AppBundle:TemplateBlockLanguageSpecifics')
                    ->findBy(['language' => $current])
            ]);
        }
        if ($page->getStaticPage()->getType() == StaticPage::TYPE_FAQ) {
            /** @var FaqLanguageSpecific[] $questions */
            $questions = $this->getDoctrine()->getRepository('InnowaitFeedbackBundle:FaqLanguageSpecific')
                ->findBy(['language' => $current]);
            return $this->render('page/faq.page.html.twig', [
                'page' => $page->getStaticPage(),
                'questions' => $questions
            ]);
        }
        if ($page->getStaticPage()->getType() == StaticPage::TYPE_SERVICES) {
            $services = $this->getDoctrine()->getRepository('CatalogBundle:ItemLanguageSpecifics')
                ->findBy(['language' => $current]);
            return $this->render('page/services.page.html.twig', [
                'page' => $page->getStaticPage(),
                'services' => $services,
                'selectedService' => $request->get('service')
            ]);
        }
        if ($page->getStaticPage()->getType() == StaticPage::TYPE_TEAM) {
            /** @var EntityRepository $teamRepo */
            $teamRepo = $this->getDoctrine()->getRepository('ApplicationBundle:StaffApplicationLanguageSpecific');

            $staff = $teamRepo->createQueryBuilder('team')
                ->select('team')
                ->leftJoin('team.staffApplication', 'application')
                ->where('team.language = :language')
                ->orderBy('application.orderNumber', 'ASC')
                ->setParameter('language', $current)
                ->getQuery()
                ->getResult();

            return $this->render('page/team.page.html.twig', [
                'page' => $page->getStaticPage(),
                'staff' => $staff
            ]);
        }

        throw new NotFoundHttpException();
    }
}
