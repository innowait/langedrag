<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 05/07/2017
 * Time: 13:08
 */

namespace StaticPageBundle\Entity;


use AppBundle\Entity\Traits\IdEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class StaticPage
 * @package StaticPageBundle\Entity
 * @ORM\Entity(repositoryClass="StaticPageBundle\Repository\StaticPageRepository")
 * @ORM\Table()
 */
class StaticPage
{
    use IdEntity;

    const TYPE_GENERAL = 'GENERAL';
    const TYPE_ABOUT = 'ABOUT';
    const TYPE_BASIC = 'BASIC';
    const TYPE_TEAM = 'TEAM';
    const TYPE_SERVICES = 'SERVICES';
    const TYPE_FAQ = 'FAQ';

    /**
     * @var StaticPage
     *
     * @ORM\ManyToOne(targetEntity="StaticPageBundle\Entity\StaticPage", inversedBy="children")
     */
    protected $parent;

    /**
     * @var StaticPage[]
     *
     * @ORM\OneToMany(targetEntity="StaticPageBundle\Entity\StaticPage", mappedBy="parent")
     */
    protected $children = [];

    /**
     * @var StaticPageLanguageSpecific[]
     *
     * @ORM\OneToMany(targetEntity="StaticPageBundle\Entity\StaticPageLanguageSpecific", mappedBy="staticPage", cascade={"PERSIST"}, orphanRemoval=true)
     */
    protected $languageSpecifics;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $background;

    /**
     * @var string
     *
     * @ORM\Column(type="string", options={"default": "GENERAL"})
     */
    protected $type = self::TYPE_GENERAL;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     */
    protected $videos = [];

    /**
     * StaticPage constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->languageSpecifics = new ArrayCollection();
    }

    /**
     * @return StaticPageLanguageSpecific[]|ArrayCollection
     */
    public function getLanguageSpecifics()
    {
        return $this->languageSpecifics;
    }

    /**
     * @param StaticPageLanguageSpecific[] $languageSpecific
     */
    public function setLanguageSpecifics($languageSpecific)
    {
        $this->languageSpecifics = $languageSpecific;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * @param string $background
     */
    public function setBackground($background)
    {
        $this->background = $background;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return StaticPage
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param StaticPage $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return StaticPage[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param StaticPage[] $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return array
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * @param array $videos
     */
    public function setVideos($videos)
    {
        $this->videos = $videos;
    }

    public function languageData($locale)
    {
        foreach ($this->languageSpecifics as $languageSpecific) {
            if ($languageSpecific->getLanguage()->getKey() == $locale) {
                return $languageSpecific;
            }
        }
        return new StaticPageLanguageSpecific();
    }
}