<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 05/07/2017
 * Time: 13:08
 */

namespace StaticPageBundle\Entity;


use AppBundle\Entity\Traits\IdEntity;
use AppBundle\Entity\Traits\LanguageSpecific;
use AppBundle\Entity\Traits\Sluggable;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class StaticPageLanguageSpecific
 * @package StaticPageBundle\Entity
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 */
class StaticPageLanguageSpecific
{
    use IdEntity;
    use LanguageSpecific;
    use Sluggable;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $caption;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    protected $content;

    /**
     * @var StaticPage
     *
     * @ORM\ManyToOne(targetEntity="StaticPageBundle\Entity\StaticPage", inversedBy="languageSpecifics", cascade={"PERSIST"})
     */
    protected $staticPage;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return StaticPage
     */
    public function getStaticPage()
    {
        return $this->staticPage;
    }

    /**
     * @param StaticPage $staticPage
     */
    public function setStaticPage($staticPage)
    {
        $this->staticPage = $staticPage;
    }

    /**
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @param string $caption
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->slug = (new Slugify())->slugify($this->title);
    }
}