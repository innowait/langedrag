<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 05/07/2017
 * Time: 00:23
 */

namespace LanguageBundle\Service;


use AdminBundle\Model\AdminFilter;
use AdminBundle\Service\AbstractAdminService;
use LanguageBundle\Entity\Language;
use LanguageBundle\Entity\Translation;
use Symfony\Component\HttpFoundation\Request;

class TranslationAdminService extends AbstractAdminService
{
    public $templates = [
        'list' => "@Language/admin/template/translation/list.template.html.twig",
        'edit' => "@Language/admin/template/translation/edit.template.html.twig"
    ];

    public function getItems(AdminFilter $filter)
    {
        return $this->repository->search($this->languageService->getCurrentLanguage(), $filter);
    }

    /**
     * @param $object Translation[]
     * @param Request $request
     * @return mixed
     */
    public function updateItem($object, Request $request)
    {
        $languages = $this->languageService->getLanguages();
        $data = $request->request->all();
        $translationGroup = null;
        $translationKey = null;
        /** @var Language $language */
        foreach ($languages as $language) {
            if (!isset($object[$language->getKey()])) {
                $translation = new Translation();
                $translation->setLanguage($language);
                $translation->setKey($translationKey);
                $translation->setGroup($translationGroup);
                $object[$language->getKey()] = $translation;
            } else {
                $translationGroup = $object[$language->getKey()]->getGroup();
                $translationKey = $object[$language->getKey()]->getKey();
            }
            $object[$language->getKey()]->setTranslation($request->get($language->getKey()));
            $this->entityManager->persist($object[$language->getKey()]);
        }
        foreach ($object as $translation) {
            if ($translation->getKey() == null) {
                $translation->setKey($translationKey);
                $translation->setGroup($translationGroup);
                $this->entityManager->persist($translation);
            }
        }
        $this->entityManager->flush();
        return $object;
    }

    public function deleteItem(AdminFilter $filter)
    {
        $items = $this->getItem($filter);
        foreach ($items as $item) {
            $this->entityManager->remove($item);
        }
        $this->entityManager->flush();
    }

    public function getItem(AdminFilter $filter)
    {
        if ($filter->getId()) {
            $mainItem = $this->repository->findOneBy(['id' => $filter->getId()]);
        }

        $items = $this->repository->findBy(['key' => $mainItem->getKey()]);
        $return = [];
        foreach ($items as $item) {
            $return[$item->getLanguage()->getKey()] = $item;
        }
        return $return;
    }
}