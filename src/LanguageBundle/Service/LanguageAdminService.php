<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 05/07/2017
 * Time: 00:23
 */

namespace LanguageBundle\Service;


use AdminBundle\Service\AbstractAdminService;
use LanguageBundle\Entity\Language;
use Symfony\Component\HttpFoundation\Request;

class LanguageAdminService extends AbstractAdminService
{
    public $templates = [
        'list' => "@Language/admin/template/language/list.template.html.twig",
        'edit' => "@Language/admin/template/language/edit.template.html.twig"
    ];

    /**
     * @param Language $object
     * @param Request $request
     * @return mixed
     */
    public function updateItem($object, Request $request)
    {
        $data = $request->request->all();
        $object->setName($data['name']);

        if ($request->get('is_enabled') == 'on') {
            $object->setEnabled(true);
        } else {
            $object->setEnabled(false);
        }

        $this->entityManager->persist($object);
        $this->entityManager->flush();
        return $object;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function createItem(Request $request)
    {
        /** @var Language $object */
        $object = $this->initItem();
        $data = $request->request->all();

        $object->setName($data['name']);
        $object->setKey($data['key']);
        if ($request->get('is_enabled') == 'on') {
            $object->setEnabled(true);
        } else {
            $object->setEnabled(false);
        }

        $this->entityManager->persist($object);
        $this->entityManager->flush();
        return $object;
    }
}