<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 11:30
 */

namespace LanguageBundle\Service;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use LanguageBundle\Entity\Language;
use LanguageBundle\Entity\Translation;
use LanguageBundle\Entity\TranslationGroup;
use LanguageBundle\Exception\DisabledLanguageException;
use LanguageBundle\Exception\InvalidLocaleException;

class LanguageService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ArrayCollection
     */
    private $library = [];

    /**
     * @var ArrayCollection
     */
    private $languages = [];

    /**
     * @var bool|Language
     */
    private $currentLanguage = false;

    /**
     * @var ArrayCollection|array
     */
    private $groups = [];

    /**
     * LanguageService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Init Language Module
     *
     * @param $locale
     * @throws DisabledLanguageException
     * @throws InvalidLocaleException
     */
    public function init($locale)
    {
        $this->languages = $this->entityManager->getRepository('LanguageBundle:Language')->findAll();
        foreach ($this->languages as $language) {
            if ($language->getKey() == $locale) {
                if ($language->isEnabled()) {
                    $this->currentLanguage = $language;
                    break;
                }
                throw new DisabledLanguageException();
            }
        }
        if (!$this->currentLanguage) {
            throw new InvalidLocaleException($locale);
        }
        $this->loadLibrary($this->currentLanguage);
    }

    public function loadLibrary(Language $language)
    {
        /** @var Translation[] $translations */
        $translations = $this->entityManager->getRepository('LanguageBundle:Translation')->createQueryBuilder('translation')
            ->select('translation', 'translation_groups')
            ->leftJoin('translation.group', 'translation_groups')
            ->where('translation.language = :language')
            ->setParameter('language', $language)
            ->getQuery()
            ->getResult();

        $groups = $this->entityManager->getRepository('LanguageBundle:TranslationGroup')->findAll();
        foreach ($translations as $translation) {
            if (isset($this->library[$translation->getGroup()->getKey()])
                && $this->library[$translation->getGroup()->getKey()] instanceof ArrayCollection) {
                $this->library[$translation->getGroup()->getKey()]->add($translation);
            } else {
                $this->library[$translation->getGroup()->getKey()] = new ArrayCollection();
            }
            if (!isset($this->groups[$translation->getGroup()->getKey()])) {
                $this->groups[$translation->getGroup()->getKey()] = $translation->getGroup();
            }
            $this->library[$translation->getGroup()->getKey()][$translation->getKey()] = $translation;
        }

        foreach ($groups as $group) {
            if (!isset($this->groups[$group->getKey()])) {
                $this->groups[$group->getKey()] = $group;
            }
        }
    }

    /**
     * Get Translation
     *
     * @param $translationKey
     * @param string $groupKey
     * @return string
     */
    public function getTranslation($translationKey, $groupKey = 'general')
    {
        $group = $this->findGroup($groupKey);
        if (!isset($this->library[$groupKey])) {
            $this->library[$groupKey] = new ArrayCollection();
        }
        if (!isset($this->library[$groupKey][$translationKey])) {
            $return = false;
            foreach ($this->languages as $language) {
                $exist = $this->entityManager->getRepository('LanguageBundle:Translation')
                    ->findOneBy(['language' => $language, 'key' => $translationKey, 'group' => $group]);
                if (!$exist) {
                    $translation = $this->generateBlankTranslation($translationKey, $language, $group);
                    if ($language == $this->currentLanguage) {
                        $return = $translation;
                        $this->library[$groupKey][$translationKey] = $translation;
                    }
                }
            }
        } else {
            $return = $this->library[$groupKey][$translationKey];
        }

        $this->entityManager->flush();
        return $return->getTranslation();
    }

    /**
     * @param $groupKey
     * @return TranslationGroup|mixed|null
     */
    private function findGroup($groupKey)
    {
        $groupKeys = [];
        /** @var TranslationGroup $group */
        foreach ($this->groups as $group) {
            $groupKeys[] = $group->getKey();
        }
        if (!in_array($groupKey, array_values($groupKeys))) {
            $group = $this->generateBlankGroup($groupKey);
            $this->groups[$groupKey] = $group;
        } else {
            $group = $this->groups[$groupKey];
        }
        return $group;
    }

    private function generateBlankTranslation($translationKey, Language $language, TranslationGroup $group)
    {
        $translation = new Translation();
        $translation->setKey($translationKey);
        $translation->setLanguage($language);
        $translation->setGroup($group);
        $translation->setTranslation($translationKey);
        $this->entityManager->persist($translation);
        return $translation;
    }

    /**
     * Generate Blank Group
     *
     * @param $groupKey
     * @return TranslationGroup
     */
    private function generateBlankGroup($groupKey)
    {
        $group = new TranslationGroup();
        $group->setKey($groupKey);
        $group->setGroupName($groupKey);
        $this->entityManager->persist($group);
        return $group;
    }

    /**
     * @return ArrayCollection
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param ArrayCollection $languages
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return Language
     */
    public function getCurrentLanguage()
    {
        return $this->currentLanguage;
    }

    /**
     * @param Language $currentLanguage
     */
    public function setCurrentLanguage(Language $currentLanguage)
    {
        $this->currentLanguage = $currentLanguage;
    }
}