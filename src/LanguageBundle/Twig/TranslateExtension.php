<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2016
 * Time: 14:21
 */

namespace LanguageBundle\Twig;


use LanguageBundle\Service\LanguageService;
use Symfony\Component\HttpKernel\Kernel;

class TranslateExtension extends \Twig_Extension
{
    /**
     * @var LanguageService
     */
    private $languageService;

    /**
     * @var string
     */
    private $environment;

    public function __construct(LanguageService $languageService, $env)
    {
        $this->languageService = $languageService;
        $this->environment = $env;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('translate', array($this, 'translateFilter'), [
                'is_safe' => array('html')
            ]),
        );
    }

    /**
     * @param $key
     * @param $group
     * @param string $type
     * @return string
     */
    public function translateFilter($key, $group, $type = 'text')
    {
        $text = $this->languageService->getTranslation($key, $group);
        $devData = '';
        if ($this->environment == 'dev') {
            $devData = "data-translation='on' data-translation-key='{$key}' data-translation-group='{$group}' ";
        }
        switch ($type) {
            case 'text':
                return "<span class='iw-admin-text' {$devData}>{$text}</span>";
            case 'textarea':
                return str_replace("\n", "<br>", $text);
            default:
                return $text;
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'translate_extension';
    }
}