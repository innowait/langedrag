<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 11/12/2017
 * Time: 23:34
 */

namespace LanguageBundle\Repository;


use AdminBundle\Model\AdminFilter;
use Doctrine\ORM\EntityRepository;
use LanguageBundle\Entity\Language;

class TranslationRepository extends EntityRepository
{
    public function search(Language $language, AdminFilter $filter)
    {
        $query = $this->createQueryBuilder('translation')
            ->select('translation');

        if ($filter->getQuery()) {
            $query->andWhere($query->expr()->orX(
                $query->expr()->like('translation.key', ':term'),
                $query->expr()->like('translation.translation', ':term')
            ))
            ->setParameter('term', '%'.$filter->getQuery().'%')
            ->groupBy('translation.key');
        } else {
            $query->where('translation.language = :language')
                ->setParameter('language', $language);
        }
        
        return $query->getQuery()->getResult();
    }
}