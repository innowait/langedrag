<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 11:24
 */

namespace LanguageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Translation
 * @package LanguageBundle\Entity
 * @ORM\Entity(repositoryClass="LanguageBundle\Repository\TranslationRepository")
 * @ORM\Table()
 */
class Translation
{
    const TYPE_STRING = 'STRING';
    const TYPE_TEXT = 'TEXT';

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="translation_key")
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="translation_type", options={"default": "STRING"})
     */
    private $type = self::TYPE_STRING;

    /**
     * @var TranslationGroup
     *
     * @ORM\ManyToOne(targetEntity="LanguageBundle\Entity\TranslationGroup")
     */
    private $group;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="LanguageBundle\Entity\Language")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(type="text", name="translation")
     */
    private $translation;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return TranslationGroup
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param TranslationGroup $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param Language $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getTranslation()
    {
        return $this->translation;
    }

    /**
     * @param string $translation
     */
    public function setTranslation($translation)
    {
        $this->translation = $translation;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }
}