<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 11:16
 */

namespace LanguageBundle\Listener;


use LanguageBundle\Service\LanguageService;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class KernelListener
{
    private $twig;
    private $languageService;

    public function __construct(\Twig_Environment $environment, LanguageService $languageService)
    {
        $this->twig = $environment;
        $this->languageService = $languageService;
    }

    public function onKernelRequest(GetResponseEvent $e)
    {
        $request = $e->getRequest();
        $locale = $request->getLocale();
        $this->twig->addGlobal('locale', $locale);
        $this->languageService->init($locale);
        $this->twig->addGlobal('activeLanguage', $this->languageService->getCurrentLanguage());
        $this->twig->addGlobal('availableLanguages', $this->languageService->getLanguages());
    }
}