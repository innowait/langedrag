<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 11:43
 */

namespace LanguageBundle\Exception;


class DisabledLanguageException extends \Exception
{
    protected $message = 'This language is disabled';
}