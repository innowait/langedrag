<?php
/**
 * Created by IntelliJ IDEA.
 * User: kmitad
 * Date: 04/07/2017
 * Time: 11:34
 */

namespace LanguageBundle\Exception;


use Throwable;

class InvalidLocaleException extends \Exception
{
    protected $message = 'Invalid Locale: ';

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        $message = $this->message . $message;
        parent::__construct($message, $code, $previous);
    }
}