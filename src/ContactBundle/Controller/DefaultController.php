<?php

namespace ContactBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/send", name="app_contact_send", options={"expose": true})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sendFormAction(Request $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $message = $request->get('message');
        if (!$name || $name == '') {
            return new JsonResponse(['success' => false]);
        }
        if (!$phone || $phone == '') {
            return new JsonResponse(['success' => false]);
        }
        if (!$email || $email == '') {
            return new JsonResponse(['success' => false]);
        }
        if (!$message || $message == '') {
            return new JsonResponse(['success' => false]);
        }
        $body = $this->renderView("@Contact/email/contact.form.html.twig", [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'message' => $request->get('message')
        ]);

        $message = new \Swift_Message('Message From Website', $body);
        $message->setTo(['info@langehouse.se'], 'Langehouse Housing');
//        $message->setTo(['dm.kmita@gmail.com'], 'Langehouse Housing');
        $message->setFrom('hello@innowait.solutions', 'Innowait CMS - Contact Form');
        $message->setContentType('text/html');
        $message->setCharset('UTF-8');
        $this->get('mailer')->send($message);
        return new JsonResponse([
            'success' => true,
            'message' => $this->get('app.language.service')->getTranslation('message-sent', 'feedback')
        ]);
    }
}
