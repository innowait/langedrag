Innowait CMS
============

#Initialise
* Update Application Name:
    - build.xml
    - composer.json
    - deploy.php
* Create DB:
   - `php app/console doctrine:database:create`
   - `php app/console doctrine:schema:update --force`
   - Run inserts:
   ```
   INSERT INTO users (id, application_id, username, username_canonical, email, email_canonical, enabled, salt, password, last_login, locked, expired, expires_at, confirmation_token, password_requested_at, roles, credentials_expired, credentials_expire_at, name, lastname) VALUES (1, null, 'admin', 'admin', 'admin', 'admin', 1, 'q2fvncdsh28kcsogwsko0sogc84wwsg', '$2y$13$q2fvncdsh28kcsogwsko0eWUdL0LMQ4YARLJ.h514l3VVsxQoW/Km', '2018-12-25 12:25:52', 0, 0, null, null, null, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, null, null, null);
   INSERT INTO language (id, language_key, name, is_enabled) VALUES (1, 'en', 'English', 1);
   ```
* Deploy to server:
` dep deploy production `

#SCSS
To Uglify:
```
uglifycss ./web/front/assets/css/main.css ./web/front/css/front.css > ./web/front/dist/internal.min.css
```
To Compile SCSS:
```
sass ./web/front/scss/main.scss:./web/front/dist/main.css --watch
```

#Javascript
```
uglifyjs ./web/front/assets/js/util.js ./web/front/libraries/iw-translate.js ./web/front/assets/js/main.js ./web/front/assets/js/gallery.js -o ./web/front/dist/internal.min.js -c
```


#Health:
/innowait/health

#Cron Jobs
* Sitemap Generation
```
php app/console app:sitemap:generate --host=domain.com (--schema=https)
```